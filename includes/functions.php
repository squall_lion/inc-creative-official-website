<?php
function include_layout_template($template){
    include(SITE_ROOT.'/public/layouts/'.$template);
}

function redirect_to($location = NULL){
	if($location != NULL){
		header("Location:{$location}");
		exit;
	}
}
?>