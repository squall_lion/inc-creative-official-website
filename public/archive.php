<?php
// ini_set('display_startup_errors',1);
// ini_set('display_errors',1);
// error_reporting(-1);
require_once('../includes/init.php');
include_layout_template('header_archive.php');
?>


<!-- This is archive. -->



<ul class="archive_wrapper">

	<!-- NEWLY ADDED OCT 2019 -->
	<li onclick="location.href='fgf'" class="archive_project_wrapper">
		<div class="archive_project_title center_align">Food<br>Glorious<span>_</span><br>Food</div>
	</li>

	<li onclick="location.href='taranaki_tales'" class="archive_project_wrapper">
		<div class="archive_project_title center_align">taranaki<br>tales<span>_</span><br>interactive</div>
	</li>

	<li onclick="location.href='skeleton'" class="archive_project_wrapper">
		<div class="archive_project_title center_align">Skeleton<br>Moves<span>_</span><br>interactive</div>
	</li>

	<li onclick="location.href='boxthorn_cutter_animation'" class="archive_project_wrapper">
		<div class="archive_project_title center_align">boxthorn<br>cutter<span>_</span><br>animation</div>
	</li>

	<li onclick="location.href='npdc'" class="archive_project_wrapper">
		<div class="archive_project_title center_align">npdc<br>photo<span>_</span><br>booth</div>
	</li>

	<li onclick="location.href='bbb'" class="archive_project_wrapper">
		<div class="archive_project_title center_align">XVI Stories<br>interactive<span>_</span><br>Kiosk</div>
	</li>
	<!-- NEWLY ADDED OCT 2019 -->

	<li onclick="location.href='das'" class="archive_project_wrapper">
		<div class="archive_project_title center_align">DEPARTURE<br>& ARRIVAL<span>_</span><br>SYSTEM</div>
	</li>

	<li onclick="location.href='instabugs'" class="archive_project_wrapper">
		<div class="archive_project_title center_align">#instaBUGS<br>CAMOUFLAGE<span>_</span><br>interactive</div>
	</li>

	<li onclick="location.href='adu'" class="archive_project_wrapper">
		<div class="archive_project_title center_align">Artefact<br>Digitising<span>_</span><br>Unit</div>
	</li>

	<li onclick="location.href='hyundai_kiosk'" class="archive_project_wrapper">
		<div class="archive_project_title center_align">hyundai<br>interactive<span>_</span><br>display</div>
	</li>

	<li onclick="location.href='bringingithome'" class="archive_project_wrapper">
		<div class="archive_project_title center_align">taranaki ww1<br>bringing it<span>_</span><br>home</div>
	</li>

	<li onclick="location.href='grommet'" class="archive_project_wrapper">
		<div class="archive_project_title center_align">grommet<br>interactive<span>_</span><br>game</div>
	</li>

	<li onclick="location.href='pna'" class="archive_project_wrapper">
		<div class="archive_project_title center_align">e-ology<br>advertising<span>_</span><br>screen</div>
	</li>

	<li onclick="location.href='kokako'" class="archive_project_wrapper">
		<div class="archive_project_title center_align">kokako<br>interactive<span>_</span><br>game</div>
	</li>

	<li onclick="location.href='samoa'" class="archive_project_wrapper">
		<div class="archive_project_title center_align">samoa<br>interactive<span>_</span><br>kiosk</div>
	</li>

	<li onclick="location.href='gallipoli'" class="archive_project_wrapper">
		<div class="archive_project_title center_align">gallipoli<br>interactive<span>_</span><br>kiosk</div>
	</li>

	<li onclick="location.href='whanganui_doc'" class="archive_project_wrapper">
		<div class="archive_project_title center_align">The Kia<br>Wharite<span>_</span><br>videos</div>
	</li>


	<li class="copyrightArchive">
		COPYRIGHT &copy; <?php echo date("Y"); ?> INC CREATIVE NEW ZEALAND. ALL RIGHTS RESERVED.
	</li>

</ul>


</div> <!-- super-container -->

<!-- <div class="copyrightArchive">COPYRIGHT &copy; <?php echo date("Y"); ?> INC CREATIVE NEW ZEALAND. ALL RIGHTS RESERVED.</div> -->

	</body>
</html>
