<?php

	// These lines are mandatory.
	$detect = new Mobile_Detect;

	$location = null;

	
	if ($detect->isMobile()) {
	    $location = "http://m.inccreative.co.nz";
	}elseif($detect->isTablet()){
	    $location = "http://m.inccreative.co.nz";
	}else{
		$location = null;
	}

	if(isset($location)){
		header("Location:{$location}");
   		exit;
	}

	/*if(!$_SESSION['isMobile']){
	    $_SESSION['isMobile'] = $detect->isMobile();
	}*/


?>
<!DOCTYPE html>
<html>
<head>
		<title>Inc Creative</title> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="apple-mobile-web-app-status-bar-style" content="black" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo PUBLIC_PATH ?>/css/reset_styles.css">
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo PUBLIC_PATH ?>/css/helpers.css">
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo PUBLIC_PATH ?>/css/styles.css" />
		<link rel="stylesheet" type="text/css" media="all" href="css/desktop.css" />
		<link rel="shortcut icon" href="<?php echo PUBLIC_PATH ?>/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo PUBLIC_PATH ?>/favicon.ico" type="image/x-icon">
		

		<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.13.2/TweenMax.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="<?php echo PUBLIC_PATH ?>/js/jquery.cookie.js"></script>

		<script type="text/javascript">

			var ignore_popup = false;

			function set_ignore_to_true(){
					console.log("set_ignore_to_true");
					ignore_popup = true;
			}

		</script>

	<?php
		$ignore = false;
		if(isset($_COOKIE["IgnoreResoWarning"])){
			$ignore = $_COOKIE["IgnoreResoWarning"] ? true : false;
			echo '<script type="text/javascript">set_ignore_to_true();</script>';
		}
	?>


    <script type="text/javascript">
   
   			
			 $(window).resize(function() {
			  onresize();
			 });
			 

			 function onresize() {

			   stageWidth = $(window).width();
			   stageHeight = $(window).height();
			 	// console.log("hello: "+stageWidth);
			 	// console.log("height " +stageHeight);

			 if(!ignore_popup){
				if(stageWidth < 1024){
					// console.log("too small");
					$("#small-size").css("display","inline-block");
					$("body").css("overflow","hidden");
					// $("#super-container-archive").css("overflow","hidden");

				}else if(stageHeight < 768){
					// console.log("too small");
					$("#small-size").css("display","inline-block");
					$("body").css("overflow","hidden");
					// $("#super-container-archive").css("overflow","hidden");

				}else{
					$("#small-size").css("display","none");
					$("body").css("overflow","none");
					// $("#super-container-archive").css("overflow","none");

				}
			}

			  $("#close-small").click(function(){

			  	 // Setting a cookie, it will be lost on browser restart:
			    $.cookie("IgnoreResoWarning",true);
			    ignore_popup = true;

	          $("#small-size").css("display","none");
	       });

			min_ratio = .5;

			   ratio = stageWidth / 1920;

			  if(ratio > 1){
			   ratio = 1;
			  }else if(ratio < min_ratio){
			   ratio = min_ratio;
			  }


			  $(".fancybox").hover(
				  function () {
				   TweenLite.to($(this).children(".pro_thumb"), .7, {scaleX:1.15, scaleY:1.15, ease:Elastic.easeOut});
				 // console.log('in '+$(this).children('.fancybox').width());
				  },

				  function () {
				  	 console.log('out');
				   TweenLite.to($(this).children(".pro_thumb"), .2, {scaleX:1, scaleY:1});
				  }
				 );
			  
			
			TweenLite.to($('#index-wrapper'), .5, {scaleX:ratio, scaleY:ratio});

			 TweenLite.to($('.projects_title_wrapper'), .5, {scaleX:ratio, scaleY:ratio});

			 TweenLite.to($('.pro_title_container'), .5, {scaleX:(ratio+.1), scaleY:(ratio+.1)});


			 jQuery.fn.center = function () {
			    this.css("position","absolute");
			    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px");
			    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
			    return this;
			}

			$('.too-small').center();
			$('#all_projects_wrapper').center();
			$('.small-size-bg').center();

		}

		 </script>
</head>

<body onload="onresize()">

<?php if(!$ignore){?>
<div id="small-size" class="center_align">

	<div class="too-small">
	<div class="scr_small"></div>
		<h5>Oops! This webpage is designed for a screen resolution that is greater than <span>1024 x 768px</span>.<br><br>For a better viewing experience <span>please resize & refresh your browser!</span></h5>

		<div id="close-small"><h5>Don't show this again!</h5></div>
	</div>
	<div class="small-size-bg"></div>
	
</div>
<?php } ?>

<div id="super-container-archive">
	<ul class="nav_btn_wrapper noselect">
		<!-- <li class="nav_btn_list"><a class="nav_btn" href="<?php echo '../../' ?>">home</a></li> -->
		<li class="nav_btn_list"><a class="nav_btn" href="index">home</a></li>
		<li class="nav_btn_list">/</li>
		<li class="nav_btn_list"><a class="nav_btn" href="all_projects">projects</a>
			<ul>
				<li class="nav_btn_list drop_down_btn" style="padding-top:15px;"><a class="nav_btn" href="projects"><span>&#8640;</span> featured projects</a>
				<li class="nav_btn_list drop_down_btn" style="margin-top:28px;"><a class="nav_btn" href="archive"><span>&#8640;</span> archived projects</a>
				<!-- <li class="nav_btn_list drop_down_btn noselect"><a class="nav_btn" href="adu"><span>&#8640;</span> ADU</a>
				<li class="nav_btn_list drop_down_btn noselect"><a class="nav_btn" href="bbb"><span>&#8640;</span> XVI stories</a>
				<li class="nav_btn_list drop_down_btn noselect"><a class="nav_btn" href="bringingithome"><span>&#8640;</span> bringing it...</a>
				<li class="nav_btn_list drop_down_btn noselect"><a class="nav_btn" href="hyundai_kiosk"><span>&#8640;</span> Hyundai Kiosk</a>
				<li class="nav_btn_list drop_down_btn noselect"><a class="nav_btn" href="grommet"><span>&#8640;</span> grommet</a>
				<li class="nav_btn_list drop_down_btn noselect"><a class="nav_btn" href="pna"><span>&#8640;</span> E-ology...</a> -->
				<!-- <li class="nav_btn_list drop_down_btn noselect"><a class="nav_btn" href="gallipoli"><span>&#8640;</span> gallipoli</a> -->
				<!-- <li class="nav_btn_list drop_down_btn noselect"><a class="nav_btn" href="kokako"><span>&#8640;</span> kokako</a> -->
				<!-- <li class="nav_btn_list drop_down_btn"><a class="nav_btn" href="samoa"><span>&#8640;</span> samoa</a> -->
			</ul>
		<li class="nav_btn_list">/</li>
		<li class="nav_btn_list"><a class="nav_btn" href="specialty">specialty</a>
		<li class="nav_btn_list">/</li>
		<li class="nav_btn_list"><a class="nav_btn" href="quick_facts">quick facts</a>
		<li class="nav_btn_list">/</li>
		<li class="nav_btn_list"><a class="nav_btn" href="partnership">PARTNERSHIPS</a>
		<li class="nav_btn_list">/</li>
		<li class="nav_btn_list"><a class="nav_btn" href="contact">contact</a>
		</li>
	</ul>