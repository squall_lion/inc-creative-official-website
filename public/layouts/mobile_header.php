<?php
$ignore = false;
if(isset($_COOKIE["IgnoreResoWarning"])){
	$ignore = $_COOKIE["IgnoreResoWarning"] == 1 ? true : false;
}
?>
<!DOCTYPE html>
<html>
<head>
		<title>Inc Creative Mobile</title> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="apple-mobile-web-app-capable" content="no" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta name="apple-mobile-web-app-status-bar-style" content="black" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo PUBLIC_PATH ?>/css/reset_styles.css">
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo PUBLIC_PATH ?>/css/helpers.css">
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo PUBLIC_PATH ?>/css/styles.css">
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo PUBLIC_PATH ?>/css/mobile.css">
		<link rel="stylesheet" type="text/css" href="<?php echo PUBLIC_PATH ?>/css/swipebox.css">
		<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.13.2/TweenMax.min.js"></script>
		<link rel="shortcut icon" href="<?php echo PUBLIC_PATH ?>/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo PUBLIC_PATH ?>/favicon.ico" type="image/x-icon">

		<!--==========================
			||                       ||
			|| Script for push menu  ||
			||                       ||
			========================= -->

		<!--load jPushMenu.css, required-->
		<link rel="stylesheet" type="text/css" href="<?php echo PUBLIC_PATH ?>/css/jPushMenu.css" />
		<!--load jQuery, required-->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<!--load jPushMenu, required-->
		<script src="<?php echo PUBLIC_PATH ?>/js/jPushMenu.js"></script>
		
		<!--call jPushMenu, required-->
		<script>
		jQuery(document).ready(function($) {
			$('.toggle-menu').jPushMenu();
		});
		</script>
		<!--==========================
			||                       ||
			|| Script for push menu  ||
			||                       ||
			========================= -->

		<!--===========================
			||                         ||
			|| Script for page slider  ||
			||                         ||
			========================= -->
		<script src="<?php echo PUBLIC_PATH ?>/js/jquery.smooth-scroll.js"></script>
		<script> //Page Slider

				$(document).ready(function() {

		      $('nav.cbp-spmenu a').smoothScroll();

		      $('p.subnav a').click(function(event) {
		        event.preventDefault();
		        var link = this;
		        $.smoothScroll({
		          scrollTarget: link.hash
		        });
		      });

		      $('#change-speed').on('click', function() {
		        var $p1 = $('nav.cbp-spmenu a').first(),
		            p1Opts = $p1.smoothScroll('options') || {};

		        p1Opts.speed = p1Opts.speed === 1400 ? 400 : 1400;
		        $p1.smoothScroll('options', p1Opts);
		      });

		      $('button.scrollsomething').click(function() {
		        $.smoothScroll({
		          scrollElement: $('div.scrollme'),
		          scrollTarget: '#findme'
		        });
		        return false;
		      });
		      $('button.scrollhorz').click(function() {
		        $.smoothScroll({
		          direction: 'left',
		          scrollElement: $('div.scrollme'),
		          scrollTarget: '.horiz'
		        });
		        return false;
		      });

		    });
		</script>
		<!--===========================
			||                         ||
			|| Script for page slider  ||
			||                         ||
			========================= -->


		<!--===========================
			||                         ||
			||  Script for Swipe Box   ||
			||                         ||
			========================= -->
		<script src="<?php echo PUBLIC_PATH ?>/js/ios-orientationchange-fix.js"></script>
		<script src="<?php echo PUBLIC_PATH ?>/js/jquery.swipebox.js"></script>
		<script type="text/javascript">
			;( function( $ ) {

				/* Basic Gallery */
				$( '.swipebox' ).swipebox();
				
				/* Video */
				$( '.swipebox-video' ).swipebox();

				/* Dynamic Gallery */
				$( '#gallery' ).click( function( e ) {
					e.preventDefault();
					$.swipebox( [
						{ href : 'http://swipebox.csag.co/mages/image-1.jpg', title : 'My Caption' },
						{ href : 'http://swipebox.csag.co/images/image-2.jpg', title : 'My Second Caption' }
					] );
				} );

			} )( jQuery );
		</script>


		<script> // check Jquery version
		//console.log("Jquery Version: " + $().jquery);	
		</script>

</head>

<body>
<div id="jpush">


		<!-- this block is in jPushMenu.css -->
		<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right">
		
		<div id="menu-btn"><img class="toggle-menu menu-right push-body" src="<?php echo PUBLIC_PATH ?>/images/menu-btn.png"></div>
			<h1 class="toggle-menu menu-right push-body">Menu</h1>
			<a class="toggle-menu menu-right push-body" href="index.php#page1">home</a>
			<a class="toggle-menu menu-right push-body" href="index.php#page2">featured projects</a>
			<a href="archive_projects">archived projects</a>
			<a class="toggle-menu menu-right push-body" href="index.php#page3">specialty</a>
			<a href="quick_facts.php">quick facts</a>
			<a href="partnership.php">PARTNERSHIPS</a>
			<a class="toggle-menu menu-right push-body" href="index.php#page4">contact</a>
		</nav>
		<!-- this block is in jPushMenu.css -->

</div>

<div id="super-container">
