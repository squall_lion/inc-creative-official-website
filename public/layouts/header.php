<?php

	// These lines are mandatory.
	$detect = new Mobile_Detect;

	$location = null;


	if ($detect->isMobile()) {
	    $location = "http://dev.inccreative.co.nz/mobile";
	}elseif($detect->isTablet()){
	    $location = "http://dev.inccreative.co.nz/mobile";
	}else{
		$location = null;
	}

	if(isset($location)){
		header("Location:{$location}");
   		exit;
	}

	/*if(!$_SESSION['isMobile']){
	    $_SESSION['isMobile'] = $detect->isMobile();
	}*/


?>
<!DOCTYPE html>
<html>
<head>
		<title>Inc Creative</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="apple-mobile-web-app-status-bar-style" content="black" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo PUBLIC_PATH ?>/css/reset_styles.css">
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo PUBLIC_PATH ?>/css/helpers.css">
		<link rel="stylesheet" type="text/css" media="all" href="css/styles.css" />
		<link rel="stylesheet" type="text/css" media="all" href="css/desktop.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo PUBLIC_PATH ?>/css/component.css">
		<link rel="shortcut icon" href="<?php echo PUBLIC_PATH ?>/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo PUBLIC_PATH ?>/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" href="<?php echo PUBLIC_PATH ?>/css/popup.css" />


		<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.13.2/TweenMax.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="<?php echo PUBLIC_PATH ?>/js/jquery.cookie.js"></script>

		<!-- Add mousewheel plugin (this is optional) -->
		<script type="text/javascript" src="<?php echo PUBLIC_PATH ?>/lib/jquery.mousewheel-3.0.6.pack.js"></script>

		<!-- Add fancyBox main JS and CSS files -->
		<script type="text/javascript" src="<?php echo PUBLIC_PATH ?>/source/jquery.fancybox.js?v=2.1.5"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo PUBLIC_PATH ?>/source/jquery.fancybox.css?v=2.1.5" media="screen" />

		<script type="text/javascript">

			var ignore_popup = false;

			function set_ignore_to_true(){
					console.log("set_ignore_to_true");
					ignore_popup = true;
			}

			$(document).ready(function() {
					/*
					 *  Simple image gallery. Uses default settings
					 */
					 $(".fancybox").fancybox({
					'padding' 		: 	3,
					'scrolling' : 'no',
					'autoSize'   : false,
					'height' : '457'
				});
			});
		</script>

	<?php
		$ignore = false;
		if(isset($_COOKIE["IgnoreResoWarning"])){
			$ignore = $_COOKIE["IgnoreResoWarning"] ? true : false;
			echo '<script type="text/javascript">set_ignore_to_true();</script>';
		}
	?>

		    <link rel="stylesheet" type="text/css" href="<?php echo PUBLIC_PATH ?>/css/slide.css">

    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.cycle/2.99/jquery.cycle.all.js"></script>
    <script type="text/javascript">
            $(function(){
            $("#site").cycle({
                fx : "scrollHorz",
                next : ".next a",
                prev : ".prev a",
                startingSlide : 0,
                timeout : 10000
                 });
                   });
    </script>

    <script type="text/javascript">


			 $(window).resize(function() {
			  onresize();
			 });


			 function onresize() {

			   stageWidth = $(window).width();
			   stageHeight = $(window).height();
			 	// console.log("hello: "+stageWidth);
			 	// console.log("height " +stageHeight);

			 if(!ignore_popup){
				if(stageWidth < 1024){
					// console.log("too small");
					$("#small-size").css("display","inline-block");
					$("body").css("overflow","hidden");

				}else if(stageHeight < 768){
					// console.log("too small");
					$("#small-size").css("display","inline-block");
					$("body").css("overflow","hidden");

				}else{
					$("#small-size").css("display","none");
					$("body").css("overflow","none");

				}
			}

			  $("#close-small").click(function(){

			  	 // Setting a cookie, it will be lost on browser restart:
			    $.cookie("IgnoreResoWarning",true);
			    ignore_popup = true;

	          $("#small-size").css("display","none");
	       });

			min_ratio = .5;

			   ratio = stageWidth / 1920;

			  if(ratio > 1){
			   ratio = 1;
			  }else if(ratio < min_ratio){
			   ratio = min_ratio;
			  }

			/*var wrapper = $('#index-wrapper');
			var p1 = $('#page1');
			console.log(p1.width());

			var content_width = wrapper.width();// * ratio;
			//var wrapper_h = wrapper.height() * ratio;





			var left_margin = (stageWidth-content_width)/2;
			console.log("left_margin: "+left_margin);*/

			// console.log("what the hell: "+stageWidth+" ratio: "+ratio);

			// var wrapper = $("#desktop_logo_wrapper");
			// wrapper.width() * ratio;
			// wrapper.height() * ratio;

			  // var wrapper_w = wrapper.width() * ratio;
			  // var wrapper_h = wrapper.height() * ratio;

			  // var margin = -(1024-wrapper_w)/2;
			  // var margin_top = -(wrapper_ori_h-wrapper_h)/2;

			  // if(ratio >= 1){
			  //  margin = -(1024-stageWidth)/2;
			  // }

			  $(".fancybox").hover(
				  function () {
				   TweenLite.to($(this).children(".pro_thumb"), .7, {scaleX:1.15, scaleY:1.15, ease:Elastic.easeOut});
				   TweenLite.to($(this).children(".pro_thumb_2019"), .7, {scaleX:1.15, scaleY:1.15, ease:Elastic.easeOut});
				 // console.log('in '+$(this).children('.fancybox').width());
				  },

				  function () {
				  	 console.log('out');
				   TweenLite.to($(this).children(".pro_thumb"), .2, {scaleX:1, scaleY:1});
				   TweenLite.to($(this).children(".pro_thumb_2019"), .2, {scaleX:1, scaleY:1});
				  }
				 );


			TweenLite.to($('#index-wrapper'), .5, {scaleX:ratio, scaleY:ratio});

			// TweenLite.to($('.index_pro_wrapper'), 0, {scaleX:ratio, scaleY:ratio});

			 TweenLite.to($('.projects_title_wrapper'), .5, {scaleX:ratio, scaleY:ratio});//, top:margin_top+'px'});
			 //TweenLite.to($('.contact_img_wrapper'), .5, {scaleX:ratio, scaleY:ratio});

			 TweenLite.to($('.pro_title_container'), .5, {scaleX:(ratio+.1), scaleY:(ratio+.1)});//, top:margin_top+'px'});
			 //TweenLite.to($('.contact_img_wrapper'), .5, {scaleX:ratio, scaleY:ratio});


			 jQuery.fn.center = function () {
			    this.css("position","absolute");
			    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px");
			    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
			    return this;
			}

			$('.too-small').center();
			$('#all_projects_wrapper').center();

		}

		$(function(){
			symbolReplace(".pro_description");
		}) // end of function

		function symbolReplace(containerName){
			$(containerName).each(function(){
				var text = $(this).html();
				$(this).html(text.replace(/‘/gi, "&#8216;").replace(/’/gi, "&#8217;").replace(/“/gi, "&#8220;").replace(/”/gi, "&#8221;").replace(/–/gi, "&#8211;").replace(/—/gi, "&#8212;"));
			});
		} // end of symbolReplace

		 </script>
</head>

<body onload="onresize()">

<?php if(!$ignore){?>
<div id="small-size">

	<div class="too-small">
	<div class="scr_small"></div>
		<h5>Oops! This webpage is designed for a screen resolution that is greater than <span>1024 x 768px</span>.<br><br>For a better viewing experience <span>please adjust your browser size!</span></h5>

		<div id="close-small"><h5>Don't show this again!</h5></div>
	</div>
	<div class="small-size-bg"></div>

</div>
<?php } ?>

<div id="super-container">
	<ul class="nav_btn_wrapper noselect">
		<!-- <li class="nav_btn_list"><a class="nav_btn" href="<?php echo '../../' ?>">home</a></li> -->
		<li class="nav_btn_list"><a class="nav_btn" href="index">home</a></li>
		<li class="nav_btn_list">/</li>
		<li class="nav_btn_list"><a class="nav_btn" href="all_projects">projects</a>
			<ul>
				<li class="nav_btn_list drop_down_btn" style="padding-top:15px;"><a class="nav_btn" href="projects"><span>&#8640;</span> featured projects</a>
				<li class="nav_btn_list drop_down_btn" style="margin-top:28px;"><a class="nav_btn" href="archive"><span>&#8640;</span> archived projects</a>
				<!-- <li class="nav_btn_list drop_down_btn noselect"><a class="nav_btn" href="adu"><span>&#8640;</span> ADU</a>
				<li class="nav_btn_list drop_down_btn noselect"><a class="nav_btn" href="bbb"><span>&#8640;</span> XVI stories</a>
				<li class="nav_btn_list drop_down_btn noselect"><a class="nav_btn" href="bringingithome"><span>&#8640;</span> bringing it...</a>
				<li class="nav_btn_list drop_down_btn noselect"><a class="nav_btn" href="hyundai_kiosk"><span>&#8640;</span> Hyundai Kiosk</a>
				<li class="nav_btn_list drop_down_btn noselect"><a class="nav_btn" href="grommet"><span>&#8640;</span> grommet</a>
				<li class="nav_btn_list drop_down_btn noselect"><a class="nav_btn" href="pna"><span>&#8640;</span> E-ology...</a> -->
				<!-- <li class="nav_btn_list drop_down_btn noselect"><a class="nav_btn" href="gallipoli"><span>&#8640;</span> gallipoli</a> -->
				<!-- <li class="nav_btn_list drop_down_btn noselect"><a class="nav_btn" href="kokako"><span>&#8640;</span> kokako</a> -->
				<!-- <li class="nav_btn_list drop_down_btn"><a class="nav_btn" href="samoa"><span>&#8640;</span> samoa</a> -->
			</ul>
		<li class="nav_btn_list">/</li>
		<li class="nav_btn_list"><a class="nav_btn" href="specialty">specialty</a>
		<li class="nav_btn_list">/</li>
		<li class="nav_btn_list"><a class="nav_btn" href="quick_facts">quick facts</a>
		<li class="nav_btn_list">/</li>
		<li class="nav_btn_list"><a class="nav_btn" href="partnership">PARTNERSHIPS</a>
		<li class="nav_btn_list">/</li>
		<li class="nav_btn_list"><a class="nav_btn" href="contact">contact</a>
		</li>
	</ul>
