   <script type="text/javascript" src="js/popup.js"></script>
	    <script>
	      $(document).ready(function() {
	       $("#bgPopup").data("state",0);
	       $("#myButton").click(function(){
	        centerPopup();
	        loadPopup();   
	       });
	       $("#popupClose").click(function(){
	          disablePopup();
	       });
	       $(document).keypress(function(e){
	        if(e.keyCode==27) {
	          disablePopup(); 
	        }
	      });
	    });

	    //Recenter the popup on resize - Thanks @Dan Harvey [http://www.danharvey.com.au/]
	    $(window).resize(function() {
	    centerPopup();
	    });
	  </script>



		