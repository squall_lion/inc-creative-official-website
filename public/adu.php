<?php
require_once('../includes/init.php');
include_layout_template('header.php');
?>


<div id="pro_wrapper" class="noselect" style="z-index:1;">
				<div class="pro_title">Artefact Digitising<br> Unit (ADU)<br>
				<!-- <div class="pro_sub_title">Ripping yarns from the Peninsula</div> -->
				</div>
				<div class="pro_category">Interactive touch screen Windows app</div> 
				

<div class="pro_description">
	In early 2015, the Auckland War Memorial Museum (AM) launched it's Online Cenotaph - a website upon which to 'Explore and contribute to our rich biographical database of New Zealand service personnel.'...<a id="myButton" href="#"><span> READ MORE</span></a>
</div>
<div class="pro_client" onClick="window.open('http://www.aucklandmuseum.com/')" style="margin-top:2.6%;">Client: <span>Auckland War Memorial Museum, New Zealand</span></div>

<div class="thumb_wrapper noselect">
		<a class="fancybox noselect" href="images/adu_img_1_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/adu_img_1.jpg" alt="" style="margin-left:0;" /></a>
		<a class="fancybox noselect" href="images/adu_img_2_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/adu_img_2.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/adu_img_3_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/adu_img_3.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/adu_img_4_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/adu_img_4.jpg" alt="" /></a>
		<!-- <a class="fancybox noselect" href="images/adu_img_1_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/adu_img_1.jpg" alt="" /></a> -->
		<a class="fancybox noselect" target="_blank" href="http://www.stuff.co.nz/auckland/local-news/68297895/online-cenotaph-launched-to-digitise-momentos.html" data-fancybox-group="gallery"><img class="pro_thumb" src="images/adu_vid_1.jpg" alt=""/></a>  
		<a class="fancybox noselect" target="_blank" href="https://www.tvnz.co.nz/one-news/new-zealand/auckland-war-memorial-museum-looks-to-digital-legacy-6307736.html?autoPlay=4217071453001" data-fancybox-group="gallery"><img class="pro_thumb" style="background:url(images/adu_vid.jpg) no-repeat; background-size: cover;" src="images/play_icon.png" alt=""/></a> 
	</div> <!-- thumb_wrapper -->
</div> <!-- pro_wrapper -->


	<!-- this block below is in the popup.css -->

		<div id="Popup">
		      <div class="pro_description" style="margin-left:auto; margin-right:auto; max-width:600px;">
		      	In early 2015, the Auckland War Memorial Museum (AM) launched it's Online Cenotaph - a website upon which to 'Explore and contribute to our rich biographical database of New Zealand service personnel.'<br><br>In addition to the launch of the Cenotaph Database, AM commissioned INC Creative, through it's strategic partner The Orange Group, to build 9 Artefact Digitising Units (ADUs). These ADU's were designed as mobile touch-screen kiosks allowing users to contribute information to the Online Cenotaph. The key design of these Digitising Units, is the ability to either take a photo of an item by placing it in the built-in photo booth, or uploading a file via a USB stick.<br><br>The team at INC Creative not only designed and built the 'Touchscreen App', but sourced and specified all hardware as well as commissioning the services of engineers to build the aluminium kiosks - all of which when combined provided some interesting challenges. Nonetheless, creative problem solving is what we do to provide yet another successful project for a happy client.

		      </div>

		      <div class="popupClose_wrapper"><a id="popupClose"><img class="popup_close_btn" src="images/popup_close.png"
					onmouseover="this.src='images/popup_close_hover.png';"
					onmouseout="this.src='images/popup_close.png';"></a>
			  </div>  
		  </div>  <!-- popup ends here -->
		<div id="bgPopup"></div> 

		<!-- this block above is in the popup.css -->


<video autoplay loop poster="vid/adu_bg_vid.jpg" id="bgvid">
<source src="vid/adu_bg_vid.webm" type="video/webm">
<source src="vid/adu_bg_vid.mp4" type="video/mp4">
</video>


<?php include_layout_template('fancy_box_popup.php') ?>

<?php include_layout_template('footer.php') ?>