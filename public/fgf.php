<?php
require_once('../includes/init.php');
include_layout_template('header.php');
?>


<div id="pro_wrapper" class="noselect" style="z-index:1;">
				<div class="pro_title">Food Glorious Food
				<div class="pro_sub_title">An army marches on its stomach</div>
				</div>
				<div class="pro_category">INTERACTIVE & GAME</div> 
				

<div class="pro_description">
The National Army Museum Te Mata Toa in Waiouru has a long-standing relationship with Inc Creative when looking for innovative and engaging interactive exhibition content. Their latest exhibition, Food glorious food, takes the visitor away from the trenches and into the cook houses of the New Zealand Division during World War One...<a id="myButton" href="#"><span> READ MORE</span></a>
</div>
<div class="pro_client" onClick="window.open('http://www.armymuseum.co.nz/')" style="margin-top:2.6%;">Client: <span>National Army Museum, Waiouru, New Zealand</span></div>
<br>
<!-- <div class="pro_client" onClick="window.open('http://www.sciencealive.co.nz/')">IN ASSOCIATION WITH: <span>SCIENCE ALIVE, CHRISTCHURCH, New Zealand</span></div>
 -->
<div class="thumb_wrapper noselect">
		<a class="fancybox noselect" href="images/fgf_img_1_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/fgf_img_1.jpg" alt="" style="margin-left:0;" /></a>
		<a class="fancybox noselect" href="images/fgf_img_2_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/fgf_img_2.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/fgf_img_3_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/fgf_img_3.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/fgf_img_4_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/fgf_img_4.jpg" alt="" /></a>
		<a class="fancybox fancybox.iframe noselect" href="http://www.youtube.com/embed/mB8aFuwZJHE" data-fancybox-group="gallery"><img class="pro_thumb" style="background:url(images/fgf_vid_1.jpg) no-repeat; background-size: cover;" src="images/play_icon.png" alt=""/></a>
		<a class="fancybox fancybox.iframe noselect" href="http://www.youtube.com/embed/g4YEaOhfKjc" data-fancybox-group="gallery"><img class="pro_thumb" style="background:url(images/fgf_vid_2.jpg) no-repeat; background-size: cover;" src="images/play_icon.png" alt=""/></a>
	</div> <!-- thumb_wrapper -->
</div> <!-- pro_wrapper -->


	<!-- this block below is in the popup.css -->

		<div id="Popup">
			<!-- <div class="longContent" style="overflow:hidden;">  -->
			      <div class="pro_description" style="margin-left:auto; margin-right:auto; max-width:600px;">    	
						The National Army Museum Te Mata Toa in Waiouru has a long-standing relationship with Inc Creative when looking for innovative and engaging interactive exhibition content. Their latest exhibition, Food glorious food, takes the visitor away from the trenches and into the cook houses of the New Zealand Division during World War One. 
						<br><br>
						The 'Moments in Time' interactive takes the user back in time to the WW1 era to show the kinds of facilities, food and ingredients that were available during a period where modern-day commodities were scarce.
						<br><br>
						Featuring several interactive sections that will appeal to all ages and levels of interest: From a series of photo walls focusing on historic footage of recreational facilities such as the Soldiers Clubs, the Y.M.C.A and the bars & canteens serving alcohol, to a section showing recipes in a book which also features a touchscreen game presented by the character 'Corporal Ratty' - where the user 'catches' the ingredients of Hokey Pokey biscuits in a bowl that a rat knocks off the shelf when it runs past.
						<br><br>
						In addition to the design, development and building of the interactive application, Inc Creative also specified and supplied the 55 inch 'Interactive flat panel' touchscreen that was fitted into a table custom-built by the National Army Museum to compliment the exhibition space.
						<br><br>
						Food glorious food is a must see for all you foodies out there next time you're in Waiouru.
		     <!--  </div> -->
	      </div>

		      <div class="popupClose_wrapper"><a id="popupClose"><img class="popup_close_btn" src="images/popup_close.png"
					onmouseover="this.src='images/popup_close_hover.png';"
					onmouseout="this.src='images/popup_close.png';"></a>
			  </div>  
		  </div>  <!-- popup ends here -->
		<div id="bgPopup"></div> 

		<!-- this block above is in the popup.css -->


<video autoplay loop poster="vid/fgf_bg_vid.jpg" id="bgvid">
<source src="vid/fgf_bg_vid.webm" type="video/webm">
<source src="vid/fgf_bg_vid.mp4" type="video/mp4">
</video>


<?php include_layout_template('fancy_box_popup.php') ?>

<?php include_layout_template('footer.php') ?>