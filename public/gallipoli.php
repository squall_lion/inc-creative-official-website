<?php
require_once('../includes/init.php');
include_layout_template('header.php');
?>


<div id="pro_wrapper" class="noselect" style="z-index:1;">
				<div class="pro_title">Gallipoli<br>
				<div class="pro_sub_title">Ripping yarns from the Peninsula</div></div>
				<div class="pro_category">Interactive touch screen Windows app</div> 
				

<div class="pro_description">
	As part of the 'WW100' (100 years since the ANZACs Landed at Gallipoli during World War One) commemorations, the National Army Museum asked the team ant INC Creative to design a logo, print and display material as well as...<a id="myButton" href="#"><span> READ MORE</span></a>
</div>
<div class="pro_client" onClick="window.open('http://www.armymuseum.co.nz/')" style="margin-top:2.6%;">Client: <span>National Army Museum, Waiouru, New Zealand</span></div>

<div class="thumb_wrapper noselect">
		<a class="fancybox noselect" href="images/gal_img_1_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/gal_img_1.jpg" alt="" style="margin-left:0;" /></a>
		<a class="fancybox noselect" href="images/gal_img_2_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/gal_img_2.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/gal_img_3_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/gal_img_3.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/gal_img_4_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/gal_img_4.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/gal_img_5_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/gal_img_5.jpg" alt="" /></a>
		<!-- <a class="fancybox fancybox.iframe" href="http://www.youtube.com/embed/IogxlwpByDs" data-fancybox-group="gallery"><img class="pro_thumb" src="images/kokako_vid.jpg" alt=""/></a>  --> 
		<a class="fancybox fancybox.iframe noselect" href="http://www.youtube.com/embed/XNmnFzjXUgU" data-fancybox-group="gallery"><img class="pro_thumb" style="background:url(images/gal_vid.jpg) no-repeat; background-size: cover;" src="images/play_icon.png" alt=""/></a> 
	</div> <!-- thumb_wrapper -->
</div> <!-- pro_wrapper -->


	<!-- this block below is in the popup.css -->

		<div id="Popup">
		      <div class="pro_description" style="margin-left:auto; margin-right:auto; max-width:600px;">
		      	As part of the 'WW100' (100 years since the ANZACs Landed at Gallipoli during World War One) commemorations, the National Army Museum asked the team ant INC Creative to design a logo, print and display material as well as a 'simple yet engaging touchscreen interactive' as part of its 'Gallipoli - Ripping Yarns from the Peninsula' exhibition which was launched on ANZAC Day 2015. <br><br>INC worked closely with National Army Museum staff helping to present these remarkable stories in an informative and engaging exhibition. After reading-over and presenting this information, the team found themselves somewhat immersed in the stories, agreeing to attend the dawn parade in our home of Whanganui - which coincidentally was the first city in New Zealand to host a dawn parade over 80 years ago.
		      </div>

		      <div class="popupClose_wrapper"><a id="popupClose"><img class="popup_close_btn" src="images/popup_close.png"
					onmouseover="this.src='images/popup_close_hover.png';"
					onmouseout="this.src='images/popup_close.png';"></a>
			  </div>  
		  </div>  <!-- popup ends here -->
		<div id="bgPopup"></div> 

		<!-- this block above is in the popup.css -->


<video autoplay loop poster="vid/gallipoli_bg_vid.jpg" id="bgvid">
<source src="vid/gallipoli_bg_vid.webm" type="video/webm">
<source src="vid/gallipoli_bg_vid.mp4" type="video/mp4">
</video>


<?php include_layout_template('fancy_box_popup.php') ?>

<?php include_layout_template('footer.php') ?>