
 
var flashWidth; var flashHeight; var flashElement;

/* From Flash, initialize width & height of flash element */
function initialize( w, h ) {

	flashElement = document.getElementById("flashcontent");
	flashElement.style.overflow = "hidden";  //since we are displaying the flash movie now we set the overflow to hidden to get rid of the vertical scrollbar
	window.onresize = doResize;
	setFlashDimensions( w, h );
}

/* called by flash whenever the width & height of the flash movie change, for testing purposes this is called onStageResize from Flash*/
function setFlashDimensions( w, h ){

	flashWidth = w;
	flashHeight = h;
	doResize();
}

/* End From Flash */


/* To Flash */

//pass browser width / height to Flash for display only using External Interface, not completely necessary
function showDimensions( w, h ){

	//id = swf id set within swfobject, example: var so = new SWFObject("example.swf", "id", "100%", "100%", "8", "#000000");
    thisMovie("id").showDimensions( w, h );
}

function thisMovie(movieName) {

    if (navigator.appName.indexOf("Microsoft") != -1) {
        return window[movieName];
    }
    else {
        return document[movieName];
    }
}

/* End To Flash */

	
function doResize() {	

	var bw; var bh;
	//http://www.quirksmode.org/viewport/compatibility.html
	if (self.innerHeight) { // all except Explorer
		bw = self.innerWidth;
		bh = self.innerHeight;
	} else if (document.documentElement && document.documentElement.clientHeight) {	// Explorer 6 Strict Mode
		bw = document.documentElement.clientWidth;
		bh = document.documentElement.clientHeight;
	}else if (document.body) { // other Explorers
		bw = document.body.clientWidth;
		bh = document.body.clientHeight;
	}

	flashElement.style.height = bh < flashHeight ? flashHeight + "px" : "100%";
	flashElement.style.width = bw < flashWidth ? flashWidth + "px" : "100%";

	/* Removed, seemed to be the reason this was crashing Firefox on Mac. */
	//showDimensions( bw, bh );
}
	
