DeepLinkingClass = {};
DeepLinkingClass = function() {
	deepLinkingObj = this;
	this.location = location;
	this.hash = location.search || location.hash;	
	this.loop();
}
DeepLinkingClass.prototype = {
	getPath:function() {
		if(this.hash.indexOf("#") < 0){
			return "";
		} else {			
			if(this.hash.charAt(this.hash.length-1) == "/"){
				return this.hash.slice(2, this.hash.length-1);
			} else {
				return this.hash.slice(2);
			}
		}
	},
	setPath:function(path) {
		if(this.location) {
			this.hash = path;
			top.location = path;
		}
	},
	loop:function() {
		if(this.hash != (top.location.search || top.location.hash) && this.flashObj) {
			this.hash = (top.location.search || top.location.hash);
			this.flashObj.changePage(this.getPath());
		}
		setTimeout("loop()", 200);
	}
}

var deepLinkingObj;
function loop() {
	if(deepLinkingObj) {
		deepLinkingObj.loop();
	}
}

