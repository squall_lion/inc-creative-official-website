<?php
require_once('../includes/init.php');
include_layout_template('header.php');
?>


<div id="pro_wrapper" style="z-index:1;">
				<div class="pro_title">samoa<br><div class="pro_sub_title">A Great and<br>Urgent Imperial Service</div></div>
				<div class="pro_category">Interactive touch screen Windows app</div> 
				

<div class="pro_description">
	As part of the ongoing WWI centennial commemorations, the National Army Muesum recently opened an exhibition based on a little known historical event...<a id="myButton" href="#"><span> READ MORE</span></a>
</div>
<div class="pro_client" onClick="window.open('http://www.armymuseum.co.nz/')" style="margin-top:2.6%;">Client: <span>National Army Museum, Waiouru, New Zealand</span></div>

<div class="thumb_wrapper">
		<a class="fancybox" href="images/samoa_img_1_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/samoa_img_1.jpg" alt="" style="margin-left:0;"/></a>
		<a class="fancybox" href="images/samoa_img_2_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/samoa_img_2.jpg" alt="" /></a>
		<a class="fancybox" href="images/samoa_img_3_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/samoa_img_3.jpg" alt="" /></a>
		<a class="fancybox" href="images/samoa_img_4_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/samoa_img_4.jpg" alt="" /></a>
		<a class="fancybox" href="images/samoa_img_5_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/samoa_img_5.jpg" alt="" /></a>
		<a class="fancybox fancybox.iframe" href="http://www.youtube.com/embed/pgP7PgZiecY" data-fancybox-group="gallery"><img class="pro_thumb" style="background:url(images/samoa_vid.jpg) no-repeat; background-size: cover;" src="images/play_icon.png" alt=""/></a> 
	</div> <!-- thumb_wrapper -->
</div> <!-- pro_wrapper -->


	<!-- this block below is in the popup.css -->

		<div id="Popup">
		      <div class="pro_description" style="margin-left:auto; margin-right:auto; max-width:600px;">
		      	As part of the ongoing WWI centennial commemorations, the National Army Museum recently opened an exhibition based on a little known historical event. The project 'Samoa' was about New Zealand forces relieving Samoa from German occupation and the events that took place thereafter.<br><br>Without many physical artefacts to display but hundreds of fully documented scanned images in the museum's collection, our solution was to display this information in an easy access digital form. As the main control interface we used a 27&#34; touch screen. This was aided by a much larger wall mounted screen used to view the hi res images with zoom capabilities. A user would start by selecting an event from the timeline to explore further. Each event had its own set of images and narrated storyline. The images are randomly assorted in a pile, as if photographs on a table. The photographs could be rearranged by the user and/or inserted into the virtual projector;  instantly projecting the images on to the larger screen accompanied with a caption.
		      </div>

		      <div class="popupClose_wrapper"><a id="popupClose"><img class="popup_close_btn" src="images/popup_close.png"
					onmouseover="this.src='images/popup_close_hover.png';"
					onmouseout="this.src='images/popup_close.png';"></a>
			  </div>  
		  </div>  <!-- popup ends here -->
		<div id="bgPopup"></div> 

		<!-- this block above is in the popup.css -->


<video autoplay loop poster="vid/samoa_bg_vid.jpg" id="bgvid">
<source src="vid/samoa_bg_vid.webm" type="video/webm">
<source src="vid/samoa_bg_vid.mp4" type="video/mp4">
</video>


<?php include_layout_template('fancy_box_popup.php') ?>

<?php include_layout_template('footer.php') ?>