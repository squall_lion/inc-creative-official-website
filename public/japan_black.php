<?php
// ini_set('display_startup_errors',1);
// ini_set('display_errors',1);
// error_reporting(-1);
require_once('../includes/init.php');
include_layout_template('header.php');
?>

<style media="screen">
  .pro_title_container {
    margin-left: 5.5rem;
    /* outline: red solid 1px; */
  }

  .pro_text {
    font-size: 50px;
  }

  @media screen and (max-width: 1920px) and (min-width: 1367px) {
    .pro_title_container {
      margin-left: 5.5rem;
    }
  }

  @media screen and (max-width: 1366px) {
    .pro_title_container {
      margin-left: 0.5rem;
    }
  }
</style>

<!-- <div id="all_projects_wrapper"> -->
<div class="center_align">

			<div class="pro_title_container">

				<div class="pro_text">
					Japan<br>Black<span>_</span><br>Exhibition
				</div>
				<button class="btn-small btn-2 btn-2c pro-btn" onclick="location.href='black-exhibition'">explore</button>

			</div>

			<div class="pro_title_container">

				<div class="pro_text">
					Japan<br>Black<span>_</span><br>Photobooth
				</div>
				<button class="btn-small btn-2 btn-2c pro-btn" onclick="location.href='black-photobooth'">explore</button>

			</div>

			<div class="pro_title_container">

				<div class="pro_text">
					Japan<br>Black<span>_</span><br>Interactive
				</div>
				<button class="btn-small btn-2 btn-2c pro-btn" onclick="location.href='black-interactive'">explore</button>

			</div>

</div>


<?php include_layout_template('footer.php') ?>
