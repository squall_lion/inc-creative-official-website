 <?php //Video directory settings and limit the file extension
		$vid_dir = "vid/project_trailers/";
		$vid_files = array_filter(scandir($vid_dir), function($vid_item) {

		  if($vid_item[0] == '.')
		    return null;

		    $pieces = explode(".", $vid_item);
		    $ext = strtolower($pieces[count($pieces)-1]);

		    if($ext != "mp4")
		        return null;

		    return $vid_item[0] !== '.';
		});

?>
<?php
require_once('../includes/init.php');
include_layout_template('header.php');

/*$project_title_1 ="Food<br>Glorious<span>_</span><br>Food";
$project_link_1="fgf";

$project_title_2 ="taranaki<br>tales<span>_</span><br>interactive";
$project_link_2="taranaki_tales";

$project_title_3 ="Skeleton<br>Moves<span>_</span><br>interactive";
$project_link_3 ="skeleton";

$project_title_4 ="boxthorn<br>cutter<span>_</span><br>animation";
$project_link_4 ="boxthorn_cutter_animation";

$project_title_5 ="npdc<br>photo<span>_</span><br>booth";
$project_link_5 ="npdc";

$project_title_6 ="XVI Stories<br>interactive<span>_</span><br>Kiosk";
$project_link_6 ="bbb";*/

$project_title_1 ="Le<span>_</span><br>Quesnoy";
$project_link_1="le-quesnoy";

// $project_title_2 ="Japan<br>Black<span>_</span><br>Exhibition";
$project_title_2 ="Japan<span>_</span><br>Black";
$project_link_2="japan_black";

$project_title_3 ="Suzuki<br>Interactive<span>_</span><br>Display";
$project_link_3 ="suzuki";

$project_title_4 ="Tears<br>of<span>_</span><br>Greenstone";
$project_link_4 ="tog";

$project_title_5 ="NZMP<span>_</span><br>Interactive";
$project_link_5 ="nzmp";

$project_title_6 ="SPARK<span>_</span><br>spotify";
$project_link_6 ="spotify";

?>



<div id="projects_container" class="noselect">

		<div class="projects_title_wrapper">

			<div class="projects_title">
				<?php echo $project_title_1; ?>
			</div>

			<button class="btn-small btn-2 btn-2c pro-btn" onclick="location.href='<?php echo $project_link_1; ?>'">explore</button>

		</div>

		<div class="projects_title_wrapper">

			<div class="projects_title">
				<?php echo $project_title_2; ?>
			</div>

			<button class="btn-small btn-2 btn-2c pro-btn" onclick="location.href='<?php echo $project_link_2; ?>'">explore</button>

		</div>

		<div class="projects_title_wrapper">

			<div class="projects_title">
				<?php echo $project_title_3; ?>
			</div>

			<button class="btn-small btn-2 btn-2c pro-btn" onclick="location.href='<?php echo $project_link_3; ?>'">explore</button>

		</div>

		<div class="projects_title_wrapper">

			<div class="projects_title">
				<?php echo $project_title_4; ?>
			</div>

			<button class="btn-small btn-2 btn-2c pro-btn" onclick="location.href='<?php echo $project_link_4; ?>'">explore</button>

		</div>


		<div class="projects_title_wrapper" style="margin-top: 80px;">

			<div class="projects_title">
				<?php echo $project_title_5; ?>
			</div>

			<button class="btn-small btn-2 btn-2c pro-btn" onclick="location.href='<?php echo $project_link_5; ?>'">explore</button>

		</div>

		<div class="projects_title_wrapper" style="margin-top: 80px;">

			<div class="projects_title">
				<?php echo $project_title_6; ?>
			</div>

			<button class="btn-small btn-2 btn-2c pro-btn" onclick="location.href='<?php echo $project_link_6; ?>'">explore</button>

		</div>


</div>


<!-- <video autoplay loop poster="vid/bringingithome_bg_vid.jpg" id="bgvid">
<source src="vid/projects_trailer.webm" type="video/webm">
<source src="vid/projects_trailer.mp4" type="video/mp4">
</video> -->
	<script type="text/javascript">
				$(document).ready(function() {
          console.log('hello world');
				        var count = 0;
				        var arr = [<?php echo '"'.implode('","', $vid_files).'"' ?>];
				        var player=document.getElementById('bgvid');
				        player.setAttribute("src", "vid/project_trailers/"+arr[count]);

				        player.addEventListener('ended',myHandler,false);

				        function myHandler(e) {
				            if(!e)
				            { e = window.event; }
				            count++;

				            if(count > arr.length-1)
				                count = 0;

				            console.log("COUNT: " + count);

				            player.setAttribute("src", "vid/project_trailers/"+arr[count]);
				        }
				    });
    </script>

<div class="video_container">
<video src="" id="bgvid" poster="" autoplay muted>
        video not supported
    </video>
</div>

<?php include_layout_template('footer.php') ?>
