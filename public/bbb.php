<?php
require_once('../includes/init.php');
include_layout_template('header.php');
?>


<div id="pro_wrapper" class="noselect" style="z-index:1;">
				<div class="pro_title">XVI Stories<br> Interactive kiosk<br>
				<div class="pro_sub_title">Balls, Bullets and Boots</div>
				</div>
				<div class="pro_category">Interactive touch screen Windows app</div> 
				

<div class="pro_description">
	Balls, Bullets & Boots, from rugby field to battlefield is a WW100 commemorative exhibition designed and produced by the New Zealand Rugby Museum, based in Palmerston North...<a id="myButton" href="#"><span> READ MORE</span></a>
</div>
<div class="pro_client" onClick="window.open('http://rugbymuseum.co.nz/')" style="margin-top:2.6%;">Client: <span>New Zealand Rugby Museum, New Zealand</span></div>

<div class="thumb_wrapper noselect">
		<a class="fancybox noselect" href="images/bbb_img_1_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/bbb_img_1.jpg" alt="" style="margin-left:0;" /></a>
		<a class="fancybox noselect" href="images/bbb_img_2_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/bbb_img_2.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/bbb_img_3_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/bbb_img_3.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/bbb_img_4_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/bbb_img_4.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/bbb_img_5_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/bbb_img_5.jpg" alt="" /></a>
		<a class="fancybox fancybox.iframe" href="http://www.youtube.com/embed/7tVeEuovvdQ" data-fancybox-group="gallery"><img class="pro_thumb" style="background:url(images/bbb_vid.jpg) no-repeat; background-size: cover;" src="images/play_icon.png" alt=""/></a> 
	</div> <!-- thumb_wrapper -->
</div> <!-- pro_wrapper -->


	<!-- this block below is in the popup.css -->

		<div id="Popup">
		      <div class="pro_description" style="margin-left:auto; margin-right:auto; max-width:600px;">
		      	Balls, Bullets & Boots, from rugby field to battlefield is a WW100 commemorative exhibition designed and produced by the New Zealand Rugby Museum, based in Palmerston North.<br><br>This captivating exhibition, set in the scene of a typical WW1 battlefield trench, uses rugby &#8220;as the common thread to link the stories of 100 years ago with people today ... features a wide array of rare rugby images, footage and displays - including dramatised short films and touchscreen interactives.&#8221;<br><br>Working alongside the very talented and motivated exhibition team at the New Zealand Rugby Museum, INC Creative was commissioned to design a &#8216;table top&#8217; touchscreen interactive in which two 55 inch touchscreens were fitted into custom-built tables. In addition to the touchscreen interactives, Balls, Bullets & Boots also features several audio-visual displays using motion-sensor activation - the videos start playing once a visitor is detected nearby. The team at INC also specified, developed and installed the touchscreens, audio -visual and motion-sensor equipment as well as editing and formatting the audio-visual content.<br><br>Company Managing Director, Lim Kai Teng, has called New Zealand home for over 17 years now and feels particularly proud to be involved in such a project. <br><br>&#8220;Rugby is part of our kiwi culture. The pride and honour associated with both playing our national game and serving our country during a war have been beautifully weaved into the Balls, Bullets & Boots exhibition. It allows the visitor to perhaps gain a better understanding of the tremendous effect both world wars had on everyday people's lives - their families, communities, our country as a whole - all those years ago.&#8221;
		      </div>

		      <div class="popupClose_wrapper"><a id="popupClose"><img class="popup_close_btn" src="images/popup_close.png"
					onmouseover="this.src='images/popup_close_hover.png';"
					onmouseout="this.src='images/popup_close.png';"></a>
			  </div>  
		  </div>  <!-- popup ends here -->
		<div id="bgPopup"></div> 

		<!-- this block above is in the popup.css -->


<video autoplay loop poster="vid/bbb_bg_vid.jpg" id="bgvid">
<source src="vid/bbb_bg_vid.webm" type="video/webm">
<source src="vid/bbb_bg_vid.mp4" type="video/mp4">
</video>


<?php include_layout_template('fancy_box_popup.php') ?>

<?php include_layout_template('footer.php') ?>