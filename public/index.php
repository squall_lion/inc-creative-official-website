<?php
// ini_set('display_startup_errors',1);
// ini_set('display_errors',1);
// error_reporting(-1);
require_once('../includes/init.php');
include_layout_template('header.php');
?>



        <div class="prev"><a href="#">&laquo;</a></div>
        <div class="next"><a href="#">&raquo;</a></div>
 <div id="site" class="noselect">
        <div id="page1">
            <div id="index-wrapper">
                <img class="desktop_logo_wrapper" src="<?php echo PUBLIC_PATH ?>/images/desktop_logo.png">
                <img class="find_out_btn" src="<?php echo PUBLIC_PATH ?>/images/find_out.png"
                onmouseover="this.src='<?php echo PUBLIC_PATH ?>/images/find_out_hover.png';"
                onmouseout="this.src='<?php echo PUBLIC_PATH ?>/images/find_out.png';"
                onclick="location.href='quick_facts'">
                <!-- <div class="next"><a href="#">&raquo;</a></div> -->
            </div>
        </div>

        <div id="page2">

            <div class="index_pro_wrapper" style="z-index:99;" >
                <div class="index_pro"><h3>LE<span>_</span><br>Quesnoy</h3>
               </div>
                 <div class="explore-btn">
                    <button class="btn-1 btn-1c btn-0" onclick="location.href='le-quesnoy'">explore</button>
                </div>
            </div>

                 <video id="bgvid" style="z-index: -2;" autoplay loop muted>
                    <source src="vid/bg_vid-le_quesnoy.mp4" type="video/mp4">
                </video>
        </div>

        <div id="page3">

            <div class="index_pro_wrapper" style="z-index:99;" >
                <!-- <div class="index_pro"><h3>Japan<br>Black<span>_</span><br>Photobooth</h3> -->
                <div class="index_pro"><h3>Japan<span>_</span><br>Black</h3>
               </div>
                 <div class="explore-btn">
                    <button class="btn-1 btn-1c btn-0" onclick="location.href='japan_black'">explore</button>
                </div>
            </div>

                 <video id="bgvid" style="z-index: -2;" autoplay loop muted>
                   <source src="vid/jpn_photobooth_slideshow.mp4" type="video/mp4">
                </video>
        </div>


         <div id="page4">

            <div class="index_pro_wrapper" style="z-index:99;" >
                <div class="index_pro"><h3>Suzuki<br>Interactive<span>_</span><br>Display</h3>
               </div>
                 <div class="explore-btn">
                    <button class="btn-1 btn-1c btn-0" onclick="location.href='suzuki'">explore</button>
                </div>
            </div>

                 <video id="bgvid" style="z-index: -2;" autoplay loop muted>
                 <source src="vid/suzuki_slideshow.mp4" type="video/mp4">
                </video>
        </div>


        <div id="page5">

            <div class="index_pro_wrapper" style="z-index:99;" >
                <div class="index_pro"><h3>Tears<br>of<span>_</span><br>Greenstone</h3>
               </div>
                 <div class="explore-btn">
                    <button class="btn-1 btn-1c btn-0" onclick="location.href='tog'">explore</button>
                </div>
            </div>

                 <video id="bgvid" style="z-index: -2;" autoplay loop muted>
                   <source src="vid/bg_vid-tog.mp4" type="video/mp4">
                </video>
        </div>

        <div id="page6">

            <div class="index_pro_wrapper" style="z-index:99;" >
                <div class="index_pro"><h3>NZMP<span>_</span><br>Interactive</h3>
               </div>
                 <div class="explore-btn">
                    <button class="btn-1 btn-1c btn-0" onclick="location.href='nzmp'">explore</button>
                </div>
            </div>

                 <video id="bgvid" style="z-index: -2;" autoplay loop muted>
                   <source src="vid/bg_vid-nzmp.mp4" type="video/mp4">
                </video>
        </div>

        <div id="page7">

            <div class="index_pro_wrapper" style="z-index:99;" >
                <div class="index_pro"><h3>SPARK<span>_</span><br>spotify</h3>
               </div>
                 <div class="explore-btn">
                    <button class="btn-1 btn-1c btn-0" onclick="location.href='spotify'">explore</button>
                </div>
            </div>

                 <video id="bgvid" style="z-index: -2;" autoplay loop muted>
                   <source src="vid/spotify_slideshow.mp4" type="video/mp4">
                </video>
        </div>

    </div>





<?php include_layout_template('footer.php') ?>
