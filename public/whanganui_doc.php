<?php
require_once('../includes/init.php');
include_layout_template('header.php');
?>


<div id="pro_wrapper" class="noselect" style="z-index:1;">
				<div class="pro_title">The Kia Wharite Project<br><div class="pro_sub_title">Promotional Videos</div></div>
				<!-- <div class="pro_title">The Kia Wharite Project - Promotional Videos</div> -->

				<div class="pro_category">film</div> 
				

<div class="pro_description">
	The Department of Conservation Whanganui launched an online campaign in 2010/11 which coincided with their 'Kia Wharite restoration project' which started in 2008...<a id="myButton" href="#"><span> READ MORE</span></a>
</div>
<div class="pro_client" onClick="window.open('http://www.doc.govt.nz/parks-and-recreation/places-to-go/manawatu-whanganui/')" style="margin-top:2.6%;">Client: <span>The Department of Conservation Whanganui, Wanganui, New Zealand</span></div>

<div class="thumb_wrapper">
<!-- 		<a class="fancybox noselect" href="images/kokako_img_1_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/kokako_img_1.jpg" alt="" style="margin-left:0;" /></a>
		<a class="fancybox noselect" href="images/kokako_img_2_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/kokako_img_2.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/kokako_img_3_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/kokako_img_3.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/kokako_img_4_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/kokako_img_4.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/kokako_img_5_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/kokako_img_5.jpg" alt="" /></a>
		<a class="fancybox fancybox.iframe" href="http://www.youtube.com/embed/IogxlwpByDs" data-fancybox-group="gallery"><img class="pro_thumb" src="images/kokako_vid.jpg" alt=""/></a>  --> 
		<a class="fancybox fancybox.iframe noselect" href="http://www.youtube.com/embed/gHmtNI0Gl1M" data-fancybox-group="gallery"><img class="pro_thumb" style="background:url(images/wdoc_vid_1.jpg) no-repeat; background-size: cover; margin-left:0" src="images/play_icon.png" alt=""/></a> 
		<a class="fancybox fancybox.iframe noselect" href="http://www.youtube.com/embed/Lvo3FK82xTg" data-fancybox-group="gallery"><img class="pro_thumb" style="background:url(images/wdoc_vid_2.jpg) no-repeat; background-size: cover;" src="images/play_icon.png" alt=""/></a> 
		<a class="fancybox fancybox.iframe noselect" href="http://www.youtube.com/embed/rNOgK_FW7Vg" data-fancybox-group="gallery"><img class="pro_thumb" style="background:url(images/wdoc_vid_3.jpg) no-repeat; background-size: cover;" src="images/play_icon.png" alt=""/></a> 
		<a class="fancybox fancybox.iframe noselect" href="http://www.youtube.com/embed/dAXSj_of-sM" data-fancybox-group="gallery"><img class="pro_thumb" style="background:url(images/wdoc_vid_4.jpg) no-repeat; background-size: cover;" src="images/play_icon.png" alt=""/></a> 
	</div> <!-- thumb_wrapper -->
</div> <!-- pro_wrapper -->


	<!-- this block below is in the popup.css -->

		<div id="Popup">
		      <div class="pro_description" style="margin-left:auto; margin-right:auto; max-width:600px;">
		      	The Department of Conservation Whanganui launched an online campaign in 2010/11 which coincided with their 'Kia Wharite restoration project' which started in 2008. The project is a collaborative biodiversity partnership aimed at protecting whio (blue duck), western brown kiwi, and old growth forest by the combined pest control efforts of DOC, Horizons Regional Council, landowners and iwi.
				<br><br>
				Our mission was to spend several days filming the resident whio, kiwi (if we could find them) and the native fauna and flora. We were helicoptered in and dropped off at Pouri hut which is located right in the middle of the Whanganui National Park. We gathered as much footage as possible - with our batteries depleted and DV tapes full, we headed back to start the editing.
				<br><br>
				We then created several video clips that were uploaded to YouTube to ignite an online presence. They were used in classroom workshops to teach children the importance of the area and to highlight the hard work done by the Kia Wharite project - keeping the area full of biodiversity for generations to come.
		      </div>

		      <div class="popupClose_wrapper"><a id="popupClose"><img class="popup_close_btn" src="images/popup_close.png"
					onmouseover="this.src='images/popup_close_hover.png';"
					onmouseout="this.src='images/popup_close.png';"></a>
			  </div>  
		  </div>  <!-- popup ends here -->
		<div id="bgPopup"></div> 

		<!-- this block above is in the popup.css -->


<video autoplay loop poster="vid/wdoc_bg_vid.jpg" id="bgvid">
<source src="vid/wdoc_bg_vid.webm" type="video/webm">
<source src="vid/wdoc_bg_vid.mp4" type="video/mp4">
</video>


<?php include_layout_template('fancy_box_popup.php') ?>

<?php include_layout_template('footer.php') ?>