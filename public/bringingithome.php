<?php
require_once('../includes/init.php');
include_layout_template('header.php');
?>


<div id="pro_wrapper" class="noselect" style="z-index:1;">
				<div class="pro_title">bringing it home<br><div class="pro_sub_title">taranaki and world war one</div></div>
				<div class="pro_category">Interactive touch screen Windows app / website</div> 
				

<div class="pro_description">
	Puke Ariki has been a valued client of INC Creative for many years. Based in New Plymouth at the base of the majestic Mt Taranaki, the team at Puke Ariki " .. aim to inspire creativity and enrich lives by promoting the heritage of Taranaki and connecting local people and visitors to new ideas and other cultures from around the world. "...<a id="myButton" href="#"><span> READ MORE</span></a>
</div>
<div class="pro_client" onClick="window.open('http://www.pukeariki.com/')" style="margin-top:2.6%;">Client: <span>Puke Ariki Museum, New Plymouth, New Zealannd</span></div>

<div class="thumb_wrapper">
		<a class="fancybox noselect" href="images/bih_img_1_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/bih_img_1.jpg" alt="" style="margin-left:0;" /></a>
		<a class="fancybox noselect" href="images/bih_img_2_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/bih_img_2.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/bih_img_3_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/bih_img_3.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/bih_img_4_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/bih_img_4.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/bih_img_5_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/bih_img_5.jpg" alt="" /></a>
		<!-- <a class="fancybox fancybox.iframe" href="http://www.youtube.com/embed/IogxlwpByDs" data-fancybox-group="gallery"><img class="pro_thumb" src="images/kokako_vid.jpg" alt=""/></a>  --> 
		<a class="fancybox fancybox.iframe" href="http://www.youtube.com/embed/P1SWpKgGt9I" data-fancybox-group="gallery"><img class="pro_thumb" style="background:url(images/bih_vid.jpg) no-repeat; background-size: cover;" src="images/play_icon.png" alt=""/></a> 
	</div> <!-- thumb_wrapper -->
</div> <!-- pro_wrapper -->


	<!-- this block below is in the popup.css -->

		<div id="Popup">
		      <div class="pro_description" style="margin-left:auto; margin-right:auto; max-width:600px;">
		      	Puke Ariki has been a valued client of INC Creative for many years. Based in New Plymouth at the base of the majestic Mt Taranaki, the team at Puke Ariki " .. aim to inspire creativity and enrich lives by promoting the heritage of Taranaki and connecting local people and visitors to new ideas and other cultures from around the world. "<br><br>This project was no exception. Bringing it Home "... uses innocent snapshots of a cricket team, a school group and family portraits to reveal the breadth of wartime realities for Taranaki people."<br><br>It was envisaged at the planning stage that as more is discovered about the people featured in these stories, more content will be added. With this in mind the Teams at INC and Puke Ariki devised a content structure around a database - enabling Bringing it Home to not only be expandable, but also allowing it to be presented across other possible media platforms in the future.<br><br>Currently consisting of a website, the exhibition space has a touchscreen kiosk plus a duplicate display projected onto the wall in the background, allowing for on-site presentations to a wider audience.
		      </div>

		      <div class="popupClose_wrapper"><a id="popupClose"><img class="popup_close_btn" src="images/popup_close.png"
					onmouseover="this.src='images/popup_close_hover.png';"
					onmouseout="this.src='images/popup_close.png';"></a>
			  </div>  
		  </div>  <!-- popup ends here -->
		<div id="bgPopup"></div> 

		<!-- this block above is in the popup.css -->


<video autoplay loop poster="vid/bringingithome_bg_vid.jpg" id="bgvid">
<source src="vid/bringingithome_bg_vid.webm" type="video/webm">
<source src="vid/bringingithome_bg_vid.mp4" type="video/mp4">
</video>


<?php include_layout_template('fancy_box_popup.php') ?>

<?php include_layout_template('footer.php') ?>