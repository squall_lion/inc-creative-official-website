<?php
require_once('../includes/init.php');
include_layout_template('header.php');
?>


<div id="pro_wrapper" class="noselect" style="z-index:1;">
				<div class="pro_title">Kokako<br><div class="pro_sub_title">Path to Sanctuary</div></div>
				<div class="pro_category">Interactive game with arcade style controls</div> 
				

<div class="pro_description">
	'Kokako - Path to Sanctuary' is a modern 2.5D interactive game based on bringing the endangered Kokako back to the Taranaki region where the population had been reduced to just one bird ...<a id="myButton" href="#"><span> READ MORE</span></a>
</div>
<div class="pro_client" onClick="window.open('http://www.pukeariki.com/')" style="margin-top:2.6%;">Client: <span>Puke Ariki Museum, New Plymouth, New Zealand</span></div>

<div class="thumb_wrapper">
		<a class="fancybox noselect" href="images/kokako_img_1_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/kokako_img_1.jpg" alt="" style="margin-left:0;" /></a>
		<a class="fancybox noselect" href="images/kokako_img_2_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/kokako_img_2.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/kokako_img_3_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/kokako_img_3.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/kokako_img_4_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/kokako_img_4.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/kokako_img_5_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/kokako_img_5.jpg" alt="" /></a>
		<!-- <a class="fancybox fancybox.iframe" href="http://www.youtube.com/embed/IogxlwpByDs" data-fancybox-group="gallery"><img class="pro_thumb" src="images/kokako_vid.jpg" alt=""/></a>  --> 
		<a class="fancybox fancybox.iframe noselect" href="http://www.youtube.com/embed/7-x5D2-S3RM" data-fancybox-group="gallery"><img class="pro_thumb" style="background:url(images/kokako_vid.jpg) no-repeat; background-size: cover;" src="images/play_icon.png" alt=""/></a> 
	</div> <!-- thumb_wrapper -->
</div> <!-- pro_wrapper -->


	<!-- this block below is in the popup.css -->

		<div id="Popup">
		      <div class="pro_description" style="margin-left:auto; margin-right:auto; max-width:600px;">
		      	'Kokako &#45; Path to Sanctuary' is a modern 2.5D interactive game based on bringing the endangered Kokako back to the Taranaki region where the population had been reduced to just one bird, Tamanui. Tamanui was rescued from certain death by DOC and taken to a captive breeding programme at Mt Bruce Wildlife Centre where he thrived, producing many chicks. Puke Ariki and the Tiaki Te Mauri o Parininihi Trust have joined forces and had begun working on pest eradication to make his home safe again. He sadly died in 2008 but his genes live on in his progeny.<br><br>The aim of the game is to help Hotu, one of Tamanui's mokopuna, who is lost and needs to find his way back to his family. It soon becomes dark and Hotu now needs to evade the threats that like to come out at night and hunt; possums, stoats, and rats. To help explain these real life threats to a younger audience, we wrote the storyline as if writing a children's book, outlining what Hotu was facing and what he needed to do next. At the beginning of the game, the player is asked to choose a language; English or Te Reo. The storyline and all interface elements have been translated enabling the whole game to be played completely in Te Reo, which we think is really quite special.<br><br>We took the story book concept a step further by having each level start out with Hotu as a  printed character on the page in a book. The player could control the printed version of Hotu around the page, interacting with text and graphics. After reading each chapter the player would then control Hotu to jump out of the book, magically burst into life and play out that level as described in the chapter. The installation of this game is a permanent fixture found only at Puke Ariki so the controls needed to be extremely robust. We found the old school arcade joystick and buttons more suited for longevity and this type of platform game.
		      </div>

		      <div class="popupClose_wrapper"><a id="popupClose"><img class="popup_close_btn" src="images/popup_close.png"
					onmouseover="this.src='images/popup_close_hover.png';"
					onmouseout="this.src='images/popup_close.png';"></a>
			  </div>  
		  </div>  <!-- popup ends here -->
		<div id="bgPopup"></div> 

		<!-- this block above is in the popup.css -->


<video autoplay loop poster="vid/kokako_bg_vid.jpg" id="bgvid">
<source src="vid/kokako_bg_vid.webm" type="video/webm">
<source src="vid/kokako_bg_vid.mp4" type="video/mp4">
</video>


<?php include_layout_template('fancy_box_popup.php') ?>

<?php include_layout_template('footer.php') ?>