<?php
require_once('../includes/init.php');
include_layout_template('header.php');

$project_title="NPDC Photo booth";
$project_subtitle="Mountain to Sea";
$project_category="Interactive photobooth";

$project_client="New Plymouth District Council, NEW ZEALAND";
$project_client_url="http://www.newplymouthnz.com/";

$projectName_img = "npdc";
$project_YouTube_link = "KpIKOoEdZU0";

$project_description_short="What do you do if you’re a tourist in New Plymouth with limited time to check out the sights? Check out the Inc Creative photobooth that allows you to virtually visit 10 top Taranaki tourist hotspots and take home a virtual holiday snap...";

$project_description_full="When Lonely Planet’s ‘Best in Travel 2017 Guide’ named Taranaki as the second-best region in the world to visit, the marketing team at the local Council called us wanting a green-screen photobooth they could take from event to event to help capitalise on this ‘marketing gold’.
<br><br> 
The design brief was to come up with a booth experience that allows a time-short tourist to tour the region’s famous places virtually and then pose for a holiday snap against any of the 10 featured backgrounds – the snap is then emailed direct to the visitor as an e-memento of their trip. Great for sharing on social media, but also an interactive way to prompt visitors to head out and explore any of these world-class destinations themselves. 
";

?>


<div id="pro_wrapper" class="noselect" style="z-index:1;">
				<div class="pro_title"><?php echo $project_title; ?><br>
				<div class="pro_sub_title"><?php echo $project_subtitle; ?></div> 
				</div>
				<div class="pro_category"><?php echo $project_category; ?></div> 
				

<div class="pro_description"><?php 	echo $project_description_short; ?><a id="myButton" href="#"><span> READ MORE</span></a>
</div>
<div class="pro_client" onClick="window.open('<?php echo $project_client_url; ?>')" style="margin-top:2.6%;">Client: <span><?php echo $project_client ?></span></div>

<div class="thumb_wrapper noselect">
		<a class="fancybox noselect" href="images/<?php echo $projectName_img; ?>_img_1_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/<?php echo $projectName_img; ?>_img_1.jpg" alt="" style="margin-left:0;" /></a>
		<a class="fancybox noselect" href="images/<?php echo $projectName_img; ?>_img_2_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/<?php echo $projectName_img; ?>_img_2.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/<?php echo $projectName_img; ?>_img_3_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/<?php echo $projectName_img; ?>_img_3.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/<?php echo $projectName_img; ?>_img_4_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/<?php echo $projectName_img; ?>_img_4.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/<?php echo $projectName_img; ?>_img_5_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/<?php echo $projectName_img; ?>_img_5.jpg" alt="" /></a>
		<!-- <a class="fancybox fancybox.iframe noselect" href="http://www.youtube.com/embed/<?php echo $project_YouTube_link;?>" data-fancybox-group="gallery"><img class="pro_thumb" style="background:url(images/<?php echo $projectName_img; ?>_img_6.jpg) no-repeat; background-size: cover;" src="images/play_icon.png" alt=""/></a>  -->
		<!-- <a class="fancybox noselect" href="images/adu_img_1_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/adu_img_1.jpg" alt="" /></a> -->
	</div> <!-- thumb_wrapper -->
</div> <!-- pro_wrapper -->


	<!-- this block below is in the popup.css -->

		<div id="Popup">
		      <div class="pro_description" style="margin-left:auto; margin-right:auto; max-width:600px;">
		      	<?php 	echo $project_description_full; ?>
		      </div>

		      <div class="popupClose_wrapper"><a id="popupClose"><img class="popup_close_btn" src="images/popup_close.png"
					onmouseover="this.src='images/popup_close_hover.png';"
					onmouseout="this.src='images/popup_close.png';"></a>
			  </div>  
		  </div>  <!-- popup ends here -->
		<div id="bgPopup"></div> 

		<!-- this block above is in the popup.css -->


<video autoplay loop poster="vid/<?php echo $projectName_img; ?>_bg_vid.jpg" id="bgvid">
<source src="vid/<?php echo $projectName_img; ?>_bg_vid.webm" type="video/webm">
<source src="vid/<?php echo $projectName_img; ?>_bg_vid.mp4" type="video/mp4">
</video>


<?php include_layout_template('fancy_box_popup.php') ?>

<?php include_layout_template('footer.php') ?>