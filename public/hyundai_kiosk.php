<?php
require_once('../includes/init.php');
include_layout_template('header.php');
?>


<div id="pro_wrapper" class="noselect" style="z-index:1;">
				<div class="pro_title">Hyundai<br>Interactive Display<br>
				<div class="pro_sub_title">55 inch touchscreen kiosks<br>design, install & maintenance.</div>
				</div>
				<div class="pro_category">Interactive Display Panel</div> 
				

<div class="pro_description">
	Our client was looking to provide a touchscreen kiosk as a sales & marketing tool for a nationwide network of dealerships...<a id="myButton" href="#"><span> READ MORE</span></a>
</div>
<div class="pro_client" onClick="window.open('http://www.hyundai.co.nz/')" style="margin-top:2.6%;">Client: <span>Hyundai Motors New Zealand</span></div>
<br>
<div class="pro_client" onClick="window.open('http://www.theorangegroup.co.nz/#/home')">Partner: <span>The Orange Group</span></div>

<div class="thumb_wrapper noselect">
		<a class="fancybox noselect" href="images/hyu_img_1_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/hyu_img_1.jpg" alt="" style="margin-left:0;" /></a>
		<a class="fancybox noselect" href="images/hyu_img_2_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/hyu_img_2.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/hyu_img_3_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/hyu_img_3.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/hyu_img_4_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/hyu_img_4.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/hyu_img_5_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/hyu_img_5.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/hyu_img_6_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/hyu_img_6.jpg" alt="" /></a>
		<!-- <a class="fancybox fancybox.iframe noselect" href="http://www.stuff.co.nz/auckland/local-news/68297895/online-cenotaph-launched-to-digitise-momentos.html" data-fancybox-group="gallery"><img class="pro_thumb" src="images/adu_vid_1.jpg" alt=""/></a>   -->
		<!-- <a class="fancybox fancybox.iframe noselect" href="http://tvnz.co.nz/national-news/auckland-war-memorial-museum-looks-digital-legacy-video-6307736" data-fancybox-group="gallery"><img class="pro_thumb" style="background:url(images/adu_vid.jpg) no-repeat; background-size: cover;" src="images/play_icon.png" alt=""/></a>  -->
	</div> <!-- thumb_wrapper -->
</div> <!-- pro_wrapper -->


	<!-- this block below is in the popup.css -->

		<div id="Popup">
		      <div class="pro_description" style="margin-left:auto; margin-right:auto; max-width:600px;">
		      	Our client was looking to provide a touchscreen kiosk as a sales & marketing tool for a nationwide network of dealerships. Working alongside the Marketing Department at National HQ, the team at INC sourced, specified, imported and installed the 55 inch touchscreen kiosks which are currently in use throughout New Zealand. As well as assisting with initial architecture, this project required constant communication with an Eastern European software developer to build the 'App' that is now installed on the kiosks. INC also put in the leg-work during the roll-out by travelling around the country installing each of the kiosks at dealerships. An ongoing maintenance contract is in place ensuring the kiosks continue to work at optimal efficiency. The team at INC is standing behind our product 24/7.

		      </div>

		      <div class="popupClose_wrapper"><a id="popupClose"><img class="popup_close_btn" src="images/popup_close.png"
					onmouseover="this.src='images/popup_close_hover.png';"
					onmouseout="this.src='images/popup_close.png';"></a>
			  </div>  
		  </div>  <!-- popup ends here -->
		<div id="bgPopup"></div> 

		<!-- this block above is in the popup.css -->


<video autoplay loop poster="vid/hyu_bg_vid.jpg" id="bgvid">
<source src="vid/hyu_bg_vid.webm" type="video/webm">
<source src="vid/hyu_bg_vid.mp4" type="video/mp4">
</video>


<?php include_layout_template('fancy_box_popup.php') ?>

<?php include_layout_template('footer.php') ?>