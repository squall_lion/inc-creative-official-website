<?php
require_once('../../includes/init.php');
include_layout_template('mobile_header_2.php');

$mobile_project_title = "Tears of Greenstone";
// $mobile_project_subtitle = "taranaki and<br>world war one";
$mobile_project_category = "Touchscreen interactive";

$mobile_project_client = "National Army Museum, Waiouru, New Zealand";
$mobile_project_client_url = "http://www.armymuseum.co.nz/";

$mobile_projectName_img = "tog";
$moible_project_YouTube_link = "VWR5MA3NcqE";

$moible_project_description_short = "";
$moible_project_description_full = "";

?>

<div id="mobile_project_page"> <!-- bringingithome page -->
	<div class="pro-wrapper">
		<div class="pro_title"><?php echo $mobile_project_title; ?>
		<!-- <div class="pro_sub_title"><?php echo $mobile_project_subtitle; ?></div> -->
		<div class="pro_category"><?php echo $mobile_project_category; ?></div>

		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="../images/<?php echo $mobile_projectName_img; ?>_img_001_large.jpg" class="swipebox">
					<!-- <img class="thumb-img" src="../images/<?php echo $mobile_projectName_img; ?>_img_001_large.jpg" alt="image"> -->
					<img class="thumb-img" src="../images/null.png" style="background:url(../images/<?php echo $mobile_projectName_img; ?>_img_001_large.jpg) no-repeat center center; background-size: cover;">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="../images/<?php echo $mobile_projectName_img; ?>_img_002_large.jpg" class="swipebox">
					<!-- <img class="thumb-img" src="../images/<?php echo $mobile_projectName_img; ?>_img_001_large.jpg" alt="image"> -->
					<img class="thumb-img" src="../images/null.png" style="background:url(../images/<?php echo $mobile_projectName_img; ?>_img_002_large.jpg) no-repeat center center; background-size: cover;">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="../images/<?php echo $mobile_projectName_img; ?>_img_003_large.jpg" class="swipebox">
					<!-- <img class="thumb-img" src="../images/<?php echo $mobile_projectName_img; ?>_img_001_large.jpg" alt="image"> -->
					<img class="thumb-img" src="../images/null.png" style="background:url(../images/<?php echo $mobile_projectName_img; ?>_img_003_large.jpg) no-repeat center center; background-size: cover;">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="../images/<?php echo $mobile_projectName_img; ?>_img_004_large.jpg" class="swipebox">
					<!-- <img class="thumb-img" src="../images/<?php echo $mobile_projectName_img; ?>_img_001_large.jpg" alt="image"> -->
					<img class="thumb-img" src="../images/null.png" style="background:url(../images/<?php echo $mobile_projectName_img; ?>_img_004_large.jpg) no-repeat center center; background-size: cover;">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="../images/<?php echo $mobile_projectName_img; ?>_img_005_large.jpg" class="swipebox">
					<!-- <img class="thumb-img" src="../images/<?php echo $mobile_projectName_img; ?>_img_001_large.jpg" alt="image"> -->
					<img class="thumb-img" src="../images/null.png" style="background:url(../images/<?php echo $mobile_projectName_img; ?>_img_005_large.jpg) no-repeat center center; background-size: cover;">
				</a>
			</li>

			<li class="pro-thumb-list">
				<a class="swipebox" href="https://www.youtube.com/watch?v=<?php echo $moible_project_YouTube_link; ?>">
					<img class="thumb-img" style="position:absolute;" src="<?php echo PUBLIC_PATH ?>/images/play_icon.png">
          <img class="thumb-img" src="../images/null.png" style="background:url(../images/<?php echo $mobile_projectName_img; ?>_img_001_large.jpg) no-repeat center center; background-size: cover;">
				</a>
			</li>
		</ul>
		<div class="pro-description"><?php echo $moible_project_description_full; ?></div>


			<!-- <div class="pro-client"style="margin-top:2.6%;">Client: <span><?php echo $mobile_project_client; ?></span></div> -->
			<div class="pro-client" onClick="window.open('<?php echo $mobile_project_client_url; ?>')" style="margin-top:2.6%;">Client: <span><?php echo $mobile_project_client; ?></span></div>

			<!-- <a class="pro-nav-btn" href="adu.php">previous project</a>
			<a class="pro-nav-btn" href="bringingithome.php" style="margin-left:120px;">next project</a> -->

		</div>
	</div> <!-- end of pro-wrapper -->
</div><!-- end of pro-page -->

<?php include_layout_template('mobile_footer.php') ?>
