<?php
require_once('../../includes/init.php');
include_layout_template('mobile_header_2.php');
?>

<div id="gallipoli-page"> 
	<div class="pro-wrapper">
		<div class="pro_title">gallipoli<br><div class="pro_sub_title">ripping yarns<br>from the peninsula</div><div class="pro_category">Interactive touch screen Windows app</div>

		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/gal_img_1_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/gal_img_1.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/gal_img_2_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/gal_img_2.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/gal_img_3_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/gal_img_3.jpg" alt="image">
				</a>
			</li>
		</ul>
		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/gal_img_4_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/gal_img_4.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/gal_img_5_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/gal_img_5.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a class="swipebox" href="https://www.youtube.com/watch?v=XNmnFzjXUgU">
					<img class="thumb-img" style="position:absolute;" src="<?php echo PUBLIC_PATH ?>/images/play_icon.png">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/gal_vid.jpg" alt="image">
				</a>
			</li>
		</ul>
		<div class="pro-description">
		      	As part of the 'WW100' (100 years since the ANZACs Landed at Gallipoli during World War One) commemorations, the National Army Museum asked the team ant INC Creative to design a logo, print and display material as well as a 'simple yet engaging touchscreen interactive' as part of its 'Gallipoli - Ripping Yarns from the Peninsula' exhibition which was launched on ANZAC Day 2015. <br><br>INC worked closely with National Army Museum staff helping to present these remarkable stories in an informative and engaging exhibition. After reading-over and presenting this information, the team found themselves somewhat immersed in the stories, agreeing to attend the dawn parade in our home of Whanganui - which coincidentally was the first city in New Zealand to host a dawn parade over 80 years ago.
		    </div>


			<div class="pro-client" onClick="window.open('http://www.armymuseum.co.nz/')" style="margin-top:2.6%;">Client: <span>National Army Museum, New Zealand</span></div>

<!-- 			<a class="pro-nav-btn" href="bringingithome.php">previous project</a>
			<a class="pro-nav-btn" href="grommet.php" style="margin-left:120px;">next project</a> -->

		</div>
	</div> <!-- end of pro-wrapper -->
</div><!-- end of pro-page -->

<?php include_layout_template('mobile_footer.php') ?>