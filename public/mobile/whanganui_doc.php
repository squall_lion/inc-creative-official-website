<?php
require_once('../../includes/init.php');
include_layout_template('mobile_header_2.php');
?>

<div id="gallipoli-page"> 
	<div class="pro-wrapper">
		<div class="pro_title">The Kia Wharite<br>Project<br><div class="pro_sub_title">Promotional Videos</div><div class="pro_category">Film</div>

		<ul id="thumb-row">
<!-- 			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/gal_img_1_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/gal_img_1.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/gal_img_2_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/gal_img_2.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/gal_img_3_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/gal_img_3.jpg" alt="image">
				</a>
			</li>
		</ul>
		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/gal_img_4_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/gal_img_4.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/gal_img_5_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/gal_img_5.jpg" alt="image">
				</a>
			</li> -->
			<li class="pro-thumb-list">
				<a class="swipebox" href="https://www.youtube.com/watch?v=gHmtNI0Gl1M">
					<img class="thumb-img" style="position:absolute;" src="<?php echo PUBLIC_PATH ?>/images/play_icon.png">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/wdoc_vid_1.jpg" alt="image">
				</a>
			</li>

			<li class="pro-thumb-list">
				<a class="swipebox" href="https://www.youtube.com/watch?v=Lvo3FK82xTg">
					<img class="thumb-img" style="position:absolute;" src="<?php echo PUBLIC_PATH ?>/images/play_icon.png">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/wdoc_vid_2.jpg" alt="image">
				</a>
			</li>

			<li class="pro-thumb-list">
				<a class="swipebox" href="https://www.youtube.com/watch?v=rNOgK_FW7Vg">
					<img class="thumb-img" style="position:absolute;" src="<?php echo PUBLIC_PATH ?>/images/play_icon.png">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/wdoc_vid_3.jpg" alt="image">
				</a>
			</li>

			<li class="pro-thumb-list">
				<a class="swipebox" href="https://www.youtube.com/watch?v=dAXSj_of">
					<img class="thumb-img" style="position:absolute;" src="<?php echo PUBLIC_PATH ?>/images/play_icon.png">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/wdoc_vid_4.jpg" alt="image">
				</a>
			</li>

			<li class="pro-thumb-list">
				<!-- <a href="<?php echo PUBLIC_PATH ?>/images/gal_img_5_large.jpg" class="swipebox"> -->
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/inc_place_holder.jpg" alt="image">
				</a>
			</li>
			
			<li class="pro-thumb-list">
				<!-- <a href="<?php echo PUBLIC_PATH ?>/images/gal_img_5_large.jpg" class="swipebox"> -->
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/inc_place_holder.jpg" alt="image">
				</a>
			</li>
		</ul>
		<div class="pro-description">
		      	The Department of Conservation Whanganui launched an online campaign in 2010/11 which coincided with their 'Kia Wharite restoration project' which started in 2008. The project is a collaborative biodiversity partnership aimed at protecting whio (blue duck), western brown kiwi, and old growth forest by the combined pest control efforts of DOC, Horizons Regional Council, landowners and iwi.
				<br><br>
				Our mission was to spend several days filming the resident whio, kiwi (if we could find them) and the native fauna and flora. We were helicoptered in and dropped off at Pouri hut which is located right in the middle of the Whanganui National Park. We gathered as much footage as possible - with our batteries depleted and DV tapes full, we headed back to start the editing.
				<br><br>
				We then created several video clips that were uploaded to YouTube to ignite an online presence. They were used in classroom workshops to teach children the importance of the area and to highlight the hard work done by the Kia Wharite project - keeping the area full of biodiversity for generations to come.
		    </div>


			<div class="pro-client" onClick="window.open('http://www.doc.govt.nz/parks-and-recreation/places-to-go/manawatu-whanganui/')" style="margin-top:2.6%;">Client: <span>The Department of Conservation Whanganui, Wanganui, New Zealand</span></div>

<!-- 			<a class="pro-nav-btn" href="bringingithome.php">previous project</a>
			<a class="pro-nav-btn" href="grommet.php" style="margin-left:120px;">next project</a> -->

		</div>
	</div> <!-- end of pro-wrapper -->
</div><!-- end of pro-page -->

<?php include_layout_template('mobile_footer.php') ?>