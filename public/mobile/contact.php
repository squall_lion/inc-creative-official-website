<?php
require_once('../../includes/init.php');
include_layout_template('mobile_header_2.php');
?>
<div id="contact-page">
	<ul class="contact-wrapper">

<!-- 		<li class="contact-list" style="margin-top:0;">
			<a href="mailto:martin@inccreative.asia"><img class="contact-btn" src="<?php echo PUBLIC_PATH ?>/images/details_icon.png"></a>
			<div class="contact_info">
					<span>martin</span><br>
						<div style="margin-top:10px;">+60 17 727 2388<br>martin@inccreative.asia</div>
			</div>

		</li>

		<li class="contact-list">
			<a href="mailto:lim@inccreative.asia"><img class="contact-btn" src="<?php echo PUBLIC_PATH ?>/images/details_icon.png"></a>
			<div class="contact_info">
					<span>lim</span><br>
						<div style="margin-top:10px;">+64 21 140 7607<br>lim@inccreative.asia</div>
			</div>

		</li>

		<li class="contact-list">
			<a  onClick="MyWindow=window.open('https://www.google.co.nz/maps/place/18A+Bell+St,+Whanganui,+Wanganui+4500/@-39.9298903,175.0541924,17z/data=!3m1!4b1!4m2!3m1!1s0x6d4003bb29ff77ef:0x3609aa63dc02aff5','MyWindow'); return false;"><img class="contact-btn" src="<?php echo PUBLIC_PATH ?>/images/address_icon.png"></a>
			<div class="contact_info">
					<span>headquarters</span><br>
						<div style="margin-top:10px;">8A Bell Street<br>Wanganui 4500<br>New Zealand<br>+64 6 348 8073</div>
			</div>

		</li>


		<li class="contact-list">
			<a onClick="MyWindow=window.open('https://www.google.co.nz/maps/place/OverTime+IOI+Boulevard+Puchong/@3.046257,101.6217112,17z/data=!3m1!4b1!4m2!3m1!1s0x31cc4b5e4cbfce79:0xce07766d5bdf5768','MyWindow'); return false;"><img class="contact-btn" src="<?php echo PUBLIC_PATH ?>/images/address_icon.png"></a>
			<div class="contact_info">
					<span>malaysia</span><br>
						<div style="margin-top:10px;">A-1-32 IOI Boulevard<br>Jalan Kenari 7<br>Bandar Puchong Jaya<br>47100 Puchong, Selangor<br>Malaysia<br>+60 3 8079 0918</div>
			</div>

		</li> -->






		<li class="contact-list" style="margin-top:0;"><a href="skype:inccreative"><img class="contact-btn" src="<?php echo PUBLIC_PATH ?>/images/desk_phone.png"></a><br><br><h5>+64 21 140 7607<br>+64 6 348 8073</h5></li>


		<li class="contact-list"><a href="mailto:lim@inccreative.co.nz"><img class="contact-btn" src="<?php echo PUBLIC_PATH ?>/images/desk_email.png"></a><br><br><h5>lim@inccreative.co.nz<br>info@inccreative.co.nz</h5></li>


		<li class="contact-list"><a href="#" onClick="MyWindow=window.open('https://www.google.co.nz/maps/place/53+Wilson+St,+Whanganui,+4500/@-39.9349805,175.0456315,17z/data=!3m1!4b1!4m5!3m4!1s0x6d4003be5e9263c9:0x5896e1456a195c6c!8m2!3d-39.9349846!4d175.0478202','MyWindow',width=320,height=240); return false;"><img class="contact-btn" src="<?php echo PUBLIC_PATH ?>/images/desk_address.png"></a><br><br><h5>53D Wilson Street,<br>Wanganui 4500,<br>New Zealand.</h5></li>


	</ul>
</div>
<!-- contact -->
<?php include_layout_template('mobile_footer.php') ?>