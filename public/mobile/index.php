<?php
require_once('../../includes/init.php');
include_layout_template('mobile_header_2.php');
?>

<style media="screen">
	.projects-img-2019 {
		filter: grayscale(90%) brightness(20%);
	}
</style>

<div id="page1">

	<div id="logo-wrapper">
		<img class="logo" src="<?php echo PUBLIC_PATH ?>/images/desktop_logo.png">
			<div class="find-out-wrapper">
				<img class="find-out" src="<?php echo PUBLIC_PATH ?>/images/find_out.png"
				 onmouseover="this.src='<?php echo PUBLIC_PATH ?>/images/find_out_hover.png'"
				 onmouseout="this.src='<?php echo PUBLIC_PATH ?>/images/find_out.png'"
				 onclick="location.href='quick_facts.php'">
			</div>
	</div>




</div>

<div id="page2">

		<div class="projects-wrapper">
		    <a href="le-quesnoy.php"><div class="projects-title" style="width:190px;">
				<p>le<span>_    </span> <br>quesnoy</p>
			</div></a>
			    <!-- <img class="projects-img" src="<?php echo PUBLIC_PATH ?>/images/m_fgf_bg.jpg"> -->
			    <img class="projects-img projects-img-2019" src="<?php echo PUBLIC_PATH ?>/images/le_quesnoy_img_001_large.jpg">
	    </div>

		<div class="projects-wrapper">
		    <a href="japan_black.php"><div class="projects-title" style="width:190px;">
				<p>Japan<span>_    </span> <br>Black</p>
			</div></a>
			    <img class="projects-img projects-img-2019" src="<?php echo PUBLIC_PATH ?>/images/jpn_exhibition_img_003_large.jpg">
	    </div>

		<div class="projects-wrapper">
		    <a href="suzuki.php"><div class="projects-title" style="width:190px;">
				<p>Suzuki<br>Interactive<span>_    </span> <br>display</p>
			</div></a>
			    <img class="projects-img projects-img-2019" src="<?php echo PUBLIC_PATH ?>/images/suzuki_img_001_large.jpg">
	    </div>

		<div class="projects-wrapper">
		    <a href="tog.php"><div class="projects-title" style="width:190px;">
				<p>Tears<br>of<span>_    </span> <br>Greenstone</p>
			</div></a>
			    <img class="projects-img projects-img-2019" src="<?php echo PUBLIC_PATH ?>/images/tog_img_001_large.jpg">
	    </div>

		<div class="projects-wrapper">
		    <a href="nzmp.php"><div class="projects-title" style="width:190px;">
				<p>nzmp<span>_    </span> <br>Interactive</p>
			</div></a>
			    <img class="projects-img projects-img-2019" src="<?php echo PUBLIC_PATH ?>/images/nzmp_img_001_large.jpg">
	    </div>

		<div class="projects-wrapper">
		    <a href="spotify.php"><div class="projects-title" style="width:190px;">
				<p>spark<span>_    </span> <br>spotify</p>
			</div></a>
			    <img class="projects-img projects-img-2019" src="<?php echo PUBLIC_PATH ?>/images/spotify_img_004_large.jpg">
	    </div>

</div>


<div id="page3">
	<div id="page3-wrapper">
		<div class="spec-title"><h4>WE DO JUST<br>ABOUT<br>ANYTHING.<br>BUT WE'RE<br>EXPERTS IN<br>INTERACTIVE<span>_</span><br>DESIGN.</h4>
		</div>
	    <div class="find-out-wrapper-2">
			<img class="find-out" src="<?php echo PUBLIC_PATH ?>/images/find_out.png"
			 onmouseover="this.src='<?php echo PUBLIC_PATH ?>/images/find_out_hover.png'"
			 onmouseout="this.src='<?php echo PUBLIC_PATH ?>/images/find_out.png'"
			 onclick="location.href='specialty.php'">
		</div>
	</div>
	<!-- <div class="find-out-wrapper-2">
		<img class="find-out" src="images/find_out.png"
		 onmouseover="this.src='images/find_out_hover.png'"
		 onmouseout="this.src='images/find_out.png'"
		 onclick="location.href='specialty.php'">
	</div> -->
</div>

<div id="page4">
    <div id="page3-wrapper">
		<div class="spec-title" style="margin-top:80px;"><h4>make<br>contact<br>we will<br>sort you out!</h4>
		</div>
	    <div class="find-out-wrapper-2" style="top: 25em;">
			<img class="find-out" src="<?php echo PUBLIC_PATH ?>/images/find_out.png"
			 onmouseover="this.src='<?php echo PUBLIC_PATH ?>/images/find_out_hover.png'"
			 onmouseout="this.src='<?php echo PUBLIC_PATH ?>/images/find_out.png'"
			 onclick="location.href='contact.php'">
		</div>
	</div>
<!-- 	<div class="find-out-wrapper">
		<img class="find-out" src="images/find_out.png"
		 onmouseover="this.src='images/find_out_hover.png'"
		 onmouseout="this.src='images/find_out.png'"
		 onclick="location.href='contact.php'">
	</div> -->
</div>

<?php include_layout_template('mobile_footer.php') ?>
