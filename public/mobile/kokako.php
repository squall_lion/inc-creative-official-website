<?php
require_once('../../includes/init.php');
include_layout_template('mobile_header_2.php');
?>

<div id="kokako-page"> <!-- kokako page -->
	<div class="pro-wrapper">
		<div class="pro_title">Kokako<br><div class="pro_sub_title">Path to Sanctuary</div><div class="pro_category">Interactive game with arcade style controls</div>

		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/kokako_img_1_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/kokako_img_1.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/kokako_img_2_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/kokako_img_2.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/kokako_img_3_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/kokako_img_3.jpg" alt="image">
				</a>
			</li>
		</ul>
		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/kokako_img_4_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/kokako_img_4.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/kokako_img_5_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/kokako_img_5.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a class="swipebox" href="https://www.youtube.com/watch?v=7-x5D2-S3RM">
					<img class="thumb-img" style="position:absolute;" src="<?php echo PUBLIC_PATH ?>/images/play_icon.png">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/kokako_vid.jpg" alt="image">
				</a>
			</li>
		</ul>
		<div class="pro-description">
		      	'Kokako &#45; Path to Sanctuary' is a modern 2.5D interactive game based on bringing the endangered Kokako back to the Taranaki region where the population had been reduced to just one bird, Tamanui. Tamanui was rescued from certain death by DOC and taken to a captive breeding programme at Mt Bruce Wildlife Centre where he thrived, producing many chicks. Puke Ariki and the Tiaki Te Mauri o Parininihi Trust have joined forces and had begun working on pest eradication to make his home safe again. He sadly died in 2008 but his genes live on in his progeny.<br><br>The aim of the game is to help Hotu, one of Tamanui's mokopuna, who is lost and needs to find his way back to his family. It soon becomes dark and Hotu now needs to evade the threats that like to come out at night and hunt; possums, stoats, and rats. To help explain these real life threats to a younger audience, we wrote the storyline as if writing a children's book, outlining what Hotu was facing and what he needed to do next. At the beginning of the game, the player is asked to choose a language; English or Te Reo. The storyline and all interface elements have been translated enabling the whole game to be played completely in Te Reo, which we think is really quite special.<br><br>We took the story book concept a step further by having each level start out with Hotu as a  printed character on the page in a book. The player could control the printed version of Hotu around the page, interacting with text and graphics. After reading each chapter the player would then control Hotu to jump out of the book, magically burst into life and play out that level as described in the chapter. The installation of this game is a permanent fixture found only at Puke Ariki so the controls needed to be extremely robust. We found the old school arcade joystick and buttons more suited for longevity and this type of platform game.
		    </div>


			<div class="pro-client" onClick="window.open('http://www.pukeariki.com/')" style="margin-top:2.6%;">Client: <span>Puke Ariki Museum, New Zealand</span></div>

<!-- 			<a class="pro-nav-btn" href="hyundai_kiosk.php">previous project</a>
			<a class="pro-nav-btn" href="bringingithome.php" style="margin-left:120px;">next project</a> -->

		</div>
	</div> <!-- end of pro-wrapper -->
</div><!-- end of pro-page -->

<?php include_layout_template('mobile_footer.php') ?>