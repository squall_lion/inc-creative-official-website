<?php
require_once('../../includes/init.php');
include_layout_template('mobile_header_2.php');

$title_name = "skeleton";
$vid_id_1 = "20eEwudC2wU";
// $vid_id_2 = "g4YEaOhfKjc";
?>

<div id="bringingithome-page"> <!-- bringingithome page -->
	<div class="pro-wrapper">
		<div class="pro_title">Skeleton Moves<br>
		<div class="pro_sub_title">KINECT MOTION SENSOR CAMERA<br>INTERACTIVE DISPLAYING ON A 65&#34; SCREEN DESIGN & INSTALL.</div>
		<div class="pro_category">Interactive game</div> 

		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/<?php echo $title_name;?>_img_1_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/<?php echo $title_name;?>_img_1.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/<?php echo $title_name;?>_img_2_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/<?php echo $title_name;?>_img_2.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/<?php echo $title_name;?>_img_3_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/<?php echo $title_name;?>_img_3.jpg" alt="image">
				</a>
			</li>
		</ul>
		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/instabugs_img_4_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/<?php echo $title_name;?>_img_4.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a onClick="window.open('http://www.thestar.com.my/videos/2016/04/29/defy-newton-law-of-motion-at-tech-dome-penang/')">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/<?php echo $title_name;?>_img_6.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a class="swipebox" href="https://www.youtube.com/watch?v=<?php echo $vid_id_1;?>">
					<img class="thumb-img" style="position:absolute;" src="<?php echo PUBLIC_PATH ?>/images/play_icon.png">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/<?php echo $title_name;?>_img_5.jpg" alt="image">
				</a>
				<!-- <a onClick="window.open('http://tvnz.co.nz/national-news/auckland-war-memorial-museum-looks-digital-legacy-video-6307736')">
					<img class="thumb-img" style="position:absolute;" src="<?php echo PUBLIC_PATH ?>/images/play_icon.png">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/adu_vid.jpg" alt="image">
				</a> -->
			</li>
		</ul>
		<div class="pro-description">
					Our brief was to design & develop an interactive aimed at a younger audience, allowing them to gain an understanding of how a human skeleton moves, and the connection between the bones.
					<br><br>
					Using Kinect Technology as the primary interactive mechanism, the team at Inc Creative designed the interface - which displayed in both English and Malay languages, completed the rigging of the 3D model skeleton and developed the software to run it.
					<br><br>
					Our lead developer Mike Abernethy also travelled to Malaysia to install the interactive at the Tech Dome in Penang. Since it is a long way to travel for servicing and maintenance, he also developed software that also allows us to monitor the system remotely from NZ.
					<br><br>
					The Skelton Moves is an entertaining, educational and highly engaging interactive. In just a few moments of movement, you find yourself being fascinated by just how much of a physical work-of-art your body really is!
		    </div>


			<!-- <div class="pro-client" onClick="window.open('http://www.aucklandmuseum.com/')" style="margin-top:2.6%;">Client: <span>Auckland War Memorial Museum, New Zealand</span></div> -->
			<div class="pro-client" onClick="window.open('http://techdomepenang.org/')" style="margin-top:2.6%;">Client: <span>TECH DOME PENANG, MALAYSIA</span></div>
			<div class="pro-client" onClick="window.open('http://www.sciencealive.co.nz/')">IN ASSOCIATION WITH: <span>SCIENCE ALIVE, CHRISTCHURCH, New Zealand</span></div>


			<!-- <a class="pro-nav-btn" href="pna.php">previous project</a> -->
			<!-- <a class="pro-nav-btn" href="bbb.php" style="margin-left:120px;">next project</a> -->

		</div>
	</div> <!-- end of pro-wrapper -->
</div><!-- end of pro-page -->

<?php include_layout_template('mobile_footer.php') ?>