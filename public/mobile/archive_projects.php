<?php
require_once('../../includes/init.php');
include_layout_template('mobile_header_2.php');
?>

<div id="grommet-page"> <!-- grommet page -->

	<div class="partnership_wrapper noselect">
		<div class="nd_title"><h4>ARCHIVED<br>PROJECTS<span>_</span></h4></div>

		<!-- <div class="archive_projects_wrapper" style="margin-top:30px;" onclick="location.href='le-quesnoy'">
			<div class="archive_projects_title center_align">le<span>_</span><br>quesnoy</div>
		</div> -->

		<div class="archive_projects_wrapper" style="margin-top:30px;" onclick="location.href='fgf'">
			<div class="archive_projects_title center_align">Food<br>Glorious<span>_</span> <br>Food</div>
		</div>

		<div class="archive_projects_wrapper" style="margin-top:30px;" onclick="location.href='das'">
			<div class="archive_projects_title center_align">DEPARTURE<br>& ARRIVAL<span>_</span> <br>SYSTEM</div>
		</div>

		<div class="archive_projects_wrapper" onclick="location.href='skeleton'">
			<div class="archive_projects_title center_align">Skeleton<br>Moves<span>_</span> <br>interactive</div>
		</div>

		<div class="archive_projects_wrapper" onclick="location.href='instabugs'">
			<div class="archive_projects_title center_align">#instaBUGS<br>CAMOUFLAGE<span>_</span> <br>interactive</div>
		</div>

		<div class="archive_projects_wrapper" onclick="location.href='adu'">
			<div class="archive_projects_title center_align">Artefact<br>Digitising<span>_</span> <br>Unit</div>
		</div>

		<div class="archive_projects_wrapper" onclick="location.href='bbb'">
			<div class="archive_projects_title center_align">XVI Stories<br>interactive<span>_</span> <br>Kiosk</div>
		</div>

		<div class="archive_projects_wrapper" onclick="location.href='hyundai_kiosk'">
			<div class="archive_projects_title center_align">hyundai<br>interactive<span>_</span><br>display</div>
		</div>

		<div class="archive_projects_wrapper" onclick="location.href='bringingithome'">
			<div class="archive_projects_title center_align">taranaki<br>ww1<br>bringing it<span>_</span><br>home</div>
		</div>

		<div class="archive_projects_wrapper" onclick="location.href='grommet'">
			<div class="archive_projects_title center_align">grommet<br>interactive<span>_</span><br>game</div>
		</div>

		<div class="archive_projects_wrapper" onclick="location.href='pna'">
			<div class="archive_projects_title center_align">e-ology<br>advertising<span>_</span><br>screen</div>
		</div>


		<div class="archive_projects_wrapper" onclick="location.href='kokako'">
			<div class="archive_projects_title center_align">kokako<br>interactive<span>_</span><br>game</div>
		</div>

		<div class="archive_projects_wrapper" onclick="location.href='samoa'">
			<div class="archive_projects_title center_align">samoa<br>interactive<span>_</span><br>kiosk</div>
		</div>

		<div class="archive_projects_wrapper" onclick="location.href='gallipoli'">
			<div class="archive_projects_title center_align">gallipoli<br>interactive<span>_</span><br>kiosk</div>
		</div>

		<div class="archive_projects_wrapper" onclick="location.href='whanganui_doc'">
			<div class="archive_projects_title center_align">the kia<br>wharite<span>_</span><br>videos</div>
		</div>

	</div>
</div>

<!-- contact -->
<?php include_layout_template('mobile_footer.php') ?>
