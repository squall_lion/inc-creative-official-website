<?php
require_once('../../includes/init.php');
include_layout_template('mobile_header_2.php');
?>

<div id="bringingithome-page"> <!-- bringingithome page -->
	<div class="pro-wrapper">
		<div class="pro_title">'DAS' DEPARTURE <br> & ARRIVAL SYSTEM <br>
		<div class="pro_sub_title">AIRPORT FLIGHT INFORMATION SCREENS & APP</div>
		<div class="pro_category">INFORMATION system</div> 

		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/das_img_1_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/das_img_1.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/das_img_2_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/das_img_2.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/das_img_3_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/das_img_3.jpg" alt="image">
				</a>
			</li>
		</ul>
		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/das_img_4_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/das_img_4.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/das_img_5_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/das_img_5.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/das_img_6_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/das_img_6.jpg" alt="image">
				</a>
				<!-- <a onClick="window.open('http://tvnz.co.nz/national-news/auckland-war-memorial-museum-looks-digital-legacy-video-6307736')">
					<img class="thumb-img" style="position:absolute;" src="<?php echo PUBLIC_PATH ?>/images/play_icon.png">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/adu_vid.jpg" alt="image">
				</a> -->
			</li>
		</ul>
		<div class="pro-description">
						During recent terminal upgrades, our client identified the need for a dynamic solution as to how they displayed flight-specific information to the growing number of passengers and visitors to the Airport. After some initial consultation and planning, the 'Departure & Arrivals System' ('DAS') was devised. 
						<br><br>
						Utilising the existing advertising screens display system currently in use by E-ology, the DAS adopts the same technology based around an App that allows airline ground staff to load flight-specific information to several display screens located above the departure doors and baggage carousels in the arrivals area.
						<br><br>
						The system is operated manually by airline control staff utilising the DAS web app on portable tablets connected via WiFi.
						<br><br>
						Important departure information such as the airline, flight number, departure gate number and status can all be displayed on the selected screens above the departure doors in use.
						<br><br>
						Additionally, when flights arrive, the flight origin and number is loaded and displayed on the existing advertising screens above the baggage carousel being used by the airline - allowing the passengers to identify which carousel to retrieve their luggage from.
						<br><br>
						The team at Inc Creative designed and developed all components of the DAS, including the tablet-based web app (think of a very basic CMS) and mimicking the 'Flight info board' style in which the information displays on the respective screens. The Baggage Carousel arrival screens is also integrated with the current advertising display software in use by E-ology. 
						<br><br>
						Inc Creative also specified, procured and installed all of the hardware required to run the system.
						<br><br>
						Since the DAS requires manual input, Inc's development team also worked with Airport Management to provide training to airline ground staff on how to use the system, which includes a basic user-manual. Finally the entire system can be monitored and accessed remotely, with Inc's staff able to provide off-site service if required.
						<br><br>
						The result of this project is a fairly unique solution when considering the need for DAS to be integrated into an existing display system. When asked to comment on the potential of such a system, Managing Director Kai Teng Lim said that aside from being expandable, it is also adaptable for various uses...
						<br><br>
						"In an application where other 'visitor-specific information' needed to the broadcast at specific locations, we could adapt this system to display different information on different screens at different venues."
						<br><br>
						"It's always exciting to see how technology allows us to adapt certain systems to suit various applications... we hadn't imagined our display technology could be used in real-time at an Airport."
						<br><br>
						Kai goes on to suggest the huge potential in having such a system that allows users to present so much more than visitor-specific information.
						<br><br>
						"In addition to the information we know the viewing audience wants to see, we can also take the opportunity to promote other relevant events, goods or services on behalf of others."
						<br><br>
						"There's no reason why a system such as this couldn't be self-funding long term, since many companies know the future is digital advertising."
		    </div>


			<!-- <div class="pro-client" onClick="window.open('http://www.aucklandmuseum.com/')" style="margin-top:2.6%;">Client: <span>Auckland War Memorial Museum, New Zealand</span></div> -->
			<div class="pro-client" onClick="window.open('http://www.pnairport.co.nz/')" style="margin-top:2.6%;">Client: <span>PALMERSTON NORTH AIRPORT, New Zealand</span></div>

			<!-- <a class="pro-nav-btn" href="pna.php">previous project</a> -->
			<!-- <a class="pro-nav-btn" href="bbb.php" style="margin-left:120px;">next project</a> -->

		</div>
	</div> <!-- end of pro-wrapper -->
</div><!-- end of pro-page -->

<?php include_layout_template('mobile_footer.php') ?>