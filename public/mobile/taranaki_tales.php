<?php
require_once('../../includes/init.php');
include_layout_template('mobile_header_2.php');

$mobile_project_title = "taranaki<br>tales<br>";
$mobile_project_subtitle = "taranaki and<br>world war one";
$mobile_project_category = "Windows Universal App";

$mobile_project_client = "PUKE ARIKI MUSEUM, NEW PLYMOUTH, NEW ZEALAND";
$mobile_project_client_url = "http://pukeariki.com/";

$mobile_projectName_img = "tt";
$moible_project_YouTube_link = "KpIKOoEdZU0";

$moible_project_description_short = "";
$moible_project_description_full = "Balls, Bullets & Boots, from rugby field to battlefield is a WW100 commemorative exhibition designed and produced by the New Zealand Rugby Museum, based in Palmerston North.<br><br>This captivating exhibition, set in the scene of a typical WW1 battlefield trench, uses rugby &#8220;as the common thread to link the stories of 100 years ago with people today ... features a wide array of rare rugby images, footage and displays - including dramatised short films and touchscreen interactives.&#8221;<br><br>Working alongside the very talented and motivated exhibition team at the New Zealand Rugby Museum, INC Creative was commissioned to design a &#8216;table top&#8217; touchscreen interactive in which two 55 inch touchscreens were fitted into custom-built tables. In addition to the touchscreen interactives, Balls, Bullets & Boots also features several audio-visual displays using motion-sensor activation - the videos start playing once a visitor is detected nearby. The team at INC also specified, developed and installed the touchscreens, audio -visual and motion-sensor equipment as well as editing and formatting the audio-visual content.<br><br>Company Managing Director, Lim Kai Teng, has called New Zealand home for over 17 years now and feels particularly proud to be involved in such a project. <br><br>&#8220;Rugby is part of our kiwi culture. The pride and honour associated with both playing our national game and serving our country during a war have been beautifully weaved into the Balls, Bullets & Boots exhibition. It allows the visitor to perhaps gain a better understanding of the tremendous effect both world wars had on everyday people's lives - their families, communities, our country as a whole - all those years ago.&#8221;";
?>

<div id="mobile_project_page"> <!-- bringingithome page -->
	<div class="pro-wrapper">
		<div class="pro_title"><?php echo $mobile_project_title; ?>
		<!-- <div class="pro_sub_title"><?php echo $mobile_project_subtitle; ?></div> -->
		<div class="pro_category"><?php echo $mobile_project_category; ?></div>

		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/<?php echo $mobile_projectName_img; ?>_img_1_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/<?php echo $mobile_projectName_img; ?>_img_1.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/<?php echo $mobile_projectName_img; ?>_img_2_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/<?php echo $mobile_projectName_img; ?>_img_2.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/<?php echo $mobile_projectName_img; ?>_img_3_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/<?php echo $mobile_projectName_img; ?>_img_3.jpg" alt="image">
				</a>
			</li>
		</ul>
		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/<?php echo $mobile_projectName_img; ?>_img_4_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/<?php echo $mobile_projectName_img; ?>_img_4.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/<?php echo $mobile_projectName_img; ?>_img_5_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/<?php echo $mobile_projectName_img; ?>_img_5.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a class="swipebox" href="https://www.youtube.com/watch?v=<?php echo $moible_project_YouTube_link; ?>">
					<img class="thumb-img" style="position:absolute;" src="<?php echo PUBLIC_PATH ?>/images/play_icon.png">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/<?php echo $mobile_projectName_img; ?>_vid.jpg" alt="image">
				</a>
			</li>
		</ul>
		<div class="pro-description"><?php echo $moible_project_description_full; ?></div>


			<div class="pro-client" onClick="window.open('<?php echo $mobile_project_client_url; ?>')" style="margin-top:2.6%;">Client: <span><?php echo $mobile_project_client; ?></span></div>

			<!-- <a class="pro-nav-btn" href="adu.php">previous project</a>
			<a class="pro-nav-btn" href="bringingithome.php" style="margin-left:120px;">next project</a> -->

		</div>
	</div> <!-- end of pro-wrapper -->
</div><!-- end of pro-page -->

<?php include_layout_template('mobile_footer.php') ?>