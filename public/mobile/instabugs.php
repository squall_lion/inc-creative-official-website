<?php
require_once('../../includes/init.php');
include_layout_template('mobile_header_2.php');
?>

<div id="bringingithome-page"> <!-- bringingithome page -->
	<div class="pro-wrapper">
		<div class="pro_title">#instaBUGS
		<div class="pro_sub_title">CAMOUFLAGE YOURSELF INTERACTIVE</div>
		<div class="pro_category">Interactive Kiosk</div> 

		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/instabugs_img_1_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/instabugs_img_1.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/instabugs_img_2_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/instabugs_img_2.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/instabugs_img_3_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/instabugs_img_3.jpg" alt="image">
				</a>
			</li>
		</ul>
		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/instabugs_img_4_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/instabugs_img_4.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/instabugs_img_5_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/instabugs_img_5.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/instabugs_img_6_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/instabugs_img_6.jpg" alt="image">
				</a>
				<!-- <a onClick="window.open('http://tvnz.co.nz/national-news/auckland-war-memorial-museum-looks-digital-legacy-video-6307736')">
					<img class="thumb-img" style="position:absolute;" src="<?php echo PUBLIC_PATH ?>/images/play_icon.png">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/adu_vid.jpg" alt="image">
				</a> -->
			</li>
		</ul>
		<div class="pro-description">
						Designed to appeal to a younger audience, this educational and vibrantly colourful touch screen interactive features the camouflage defence mechanisms of 3 bugs found in NZ: The praying mantis, the stick insect and the bird-dropping spider. 
						<br><br>
						The user is invited to ‘camouflage yourself’ in an on-screen simulation similar to the camouflage appearance of the chosen bug. You can then take a ‘3-2-1, snapshot’ of your camouflaged-self and share the image on a larger screen nearby and/or by email as a mechanism to also promote the exhibit to a wider audience.
						<br><br>
						As well as being educational, the interactivity has a fun aspect with users able to wear supplied ‘props’ that match the green screen behind them to further enhance the camouflage effect reinforcing the idea that some bugs can change their appearance to blend in better with their surroundings. 
						<br><br>
						The team at Inc Creative worked closely with Puke Ariki to develop an interactive that fulfilled the brief within the budget and timeframe. An exciting element in this project is the utilisation of the 55 inch Interactive Flat Panel (IFP) which is becoming increasingly popular for anyone wanting an ‘all-in-one’ touchscreen solution.
						<br><br>
						“Our 55" IFP’s are the sister to our free-standing kiosks that are ins use throughout NZ, but obviously more portable and flexible in terms of their ability to display in either landscape or portrait.” Comments Kai Teng Lim, Managing Director, Inc Creative Limited.
						<br><br>
						Whilst specialising in Interactive Design, Inc Creative also imports many touchscreen hardware configurations at very competitive prices.
		    </div>


			<!-- <div class="pro-client" onClick="window.open('http://www.aucklandmuseum.com/')" style="margin-top:2.6%;">Client: <span>Auckland War Memorial Museum, New Zealand</span></div> -->
			<div class="pro-client" onClick="window.open('http://pukeariki.com/')" style="margin-top:2.6%;">Client: <span>PUKE ARIKI MUSEUM, NEW PLYMOUTH, NEW ZEALAND</span></div>

			<!-- <a class="pro-nav-btn" href="pna.php">previous project</a> -->
			<!-- <a class="pro-nav-btn" href="bbb.php" style="margin-left:120px;">next project</a> -->

		</div>
	</div> <!-- end of pro-wrapper -->
</div><!-- end of pro-page -->

<?php include_layout_template('mobile_footer.php') ?>