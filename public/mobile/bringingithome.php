<?php
require_once('../../includes/init.php');
include_layout_template('mobile_header_2.php');
?>

<div id="bringingithome-page"> <!-- bringingithome page -->
	<div class="pro-wrapper">
		<div class="pro_title">bringing it<br>home<br><div class="pro_sub_title">taranaki and<br>world war one</div><div class="pro_category">Interactive touch screen app / website</div>

		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/bih_img_1_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/bih_img_1.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/bih_img_2_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/bih_img_2.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/bih_img_3_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/bih_img_3.jpg" alt="image">
				</a>
			</li>
		</ul>
		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/bih_img_4_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/bih_img_4.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/bih_img_5_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/bih_img_5.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a class="swipebox" href="https://www.youtube.com/watch?v=P1SWpKgGt9I">
					<img class="thumb-img" style="position:absolute;" src="<?php echo PUBLIC_PATH ?>/images/play_icon.png">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/bih_vid.jpg" alt="image">
				</a>
			</li>
		</ul>
		<div class="pro-description">
		      	Puke Ariki has been a valued client of INC Creative for many years. Based in New Plymouth at the base of the majestic Mt Taranaki, the team at Puke Ariki " .. aim to inspire creativity and enrich lives by promoting the heritage of Taranaki and connecting local people and visitors to new ideas and other cultures from around the world. "<br><br>This project was no exception. Bringing it Home "... uses innocent snapshots of a cricket team, a school group and family portraits to reveal the breadth of wartime realities for Taranaki people."<br><br>It was envisaged at the planning stage that as more is discovered about the people featured in these stories, more content will be added. With this in mind the Teams at INC and Puke Ariki devised a content structure around a database - enabling Bringing it Home to not only be expandable, but also allowing it to be presented across other possible media platforms in the future.<br><br>Currently consisting of a website, the exhibition space has a touchscreen kiosk plus a duplicate display projected onto the wall in the background, allowing for on-site presentations to a wider audience.
		    </div>


			<div class="pro-client" onClick="window.open('http://www.pukeariki.com/')" style="margin-top:2.6%;">Client: <span>Puke Ariki Museum, New Zealand</span></div>

			<!-- <a class="pro-nav-btn" href="bbb.php">previous project</a>
			<a class="pro-nav-btn" href="grommet.php" style="margin-left:120px;">next project</a> -->

		</div>
	</div> <!-- end of pro-wrapper -->
</div><!-- end of pro-page -->

<?php include_layout_template('mobile_footer.php') ?>