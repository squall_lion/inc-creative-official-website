<?php
require_once('../../includes/init.php');
include_layout_template('mobile_header_2.php');
?>

<div id="bringingithome-page"> <!-- bringingithome page -->
	<div class="pro-wrapper">
		<div class="pro_title">Hyundai<br>Interactive<br>Display<br>
		<div class="pro_sub_title">55 inch<br>touchscreen<br>kiosks design<br>install &<br>maintenance.</div>
		<div class="pro_category">Interactive Display Panel</div>

		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/hyu_img_1_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/hyu_img_1.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/hyu_img_2_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/hyu_img_2.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/hyu_img_3_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/hyu_img_3.jpg" alt="image">
				</a>
			</li>
		</ul>
		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/hyu_img_4_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/hyu_img_4.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/hyu_img_5_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/hyu_img_5.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/hyu_img_6_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/hyu_img_6.jpg" alt="image">
				</a>
			</li>
		</ul>
		<div class="pro-description">
		      	Our client was looking to provide a touchscreen kiosk as a sales & marketing tool for a nationwide network of dealerships. Working alongside the Marketing Department at National HQ, the team at INC sourced, specified, imported and installed the 55 inch touchscreen kiosks which are currently in use throughout New Zealand. As well as assisting with initial architecture, this project required constant communication with an Eastern European software developer to build the 'App' that is now installed on the kiosks. INC also put in the leg-work during the roll-out by travelling around the country installing each of the kiosks at dealerships. An ongoing maintenance contract is in place ensuring the kiosks continue to work at optimal efficiency. The team at INC is standing behind our product 24/7.

		    </div>


			<div class="pro-client" onClick="window.open('http://www.aucklandmuseum.com/')" style="padding-bottom:-10px;;">Client: <span>Hyundai New Zealand</span></div>
			<!-- <br> -->
			<div class="pro-client" onClick="window.open('http://www.theorangegroup.co.nz/#/home')" style="">Partner: <span>The Orange Group</span></div>

			<!-- <a class="pro-nav-btn" href="grommet.php">previous project</a>
			<a class="pro-nav-btn" href="pna.php" style="margin-left:120px;">next project</a> -->

		</div>
	</div> <!-- end of pro-wrapper -->
</div><!-- end of pro-page -->

<?php include_layout_template('mobile_footer.php') ?>