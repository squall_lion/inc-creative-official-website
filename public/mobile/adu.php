<?php
require_once('../../includes/init.php');
include_layout_template('mobile_header_2.php');
?>

<div id="bringingithome-page"> <!-- bringingithome page -->
	<div class="pro-wrapper">
		<div class="pro_title">Artefact<br>Digitising<br>Unit<br>
		<!-- <div class="pro_sub_title">taranaki and<br>world war one</div> -->
		<div class="pro_category">Interactive touch screen app</div>

		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/adu_img_1_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/adu_img_1.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/adu_img_2_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/adu_img_2.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/adu_img_3_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/adu_img_3.jpg" alt="image">
				</a>
			</li>
		</ul>
		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/adu_img_4_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/adu_img_4.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a onClick="window.open('http://www.stuff.co.nz/auckland/local-news/68297895/online-cenotaph-launched-to-digitise-momentos.html')">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/adu_vid_1.jpg" alt="image">

				</a>
			</li>
			<li class="pro-thumb-list">
				<a onClick="window.open('http://tvnz.co.nz/national-news/auckland-war-memorial-museum-looks-digital-legacy-video-6307736')">
					<img class="thumb-img" style="position:absolute;" src="<?php echo PUBLIC_PATH ?>/images/play_icon.png">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/adu_vid.jpg" alt="image">
				</a>
			</li>
		</ul>
		<div class="pro-description">
		      	In early 2015, the Auckland War Memorial Museum (AM) launched it's Online Cenotaph - a website upon which to 'Explore and contribute to our rich biographical database of New Zealand service personnel.'<br><br>In addition to the launch of the Cenotaph Database, AM commissioned INC Creative, through it's strategic partner The Orange Group, to build 9 Artefact Digitising Units (ADUs). These ADU's were designed as mobile touch-screen kiosks allowing users to contribute information to the Online Cenotaph. The key design of these Digitising Units, is the ability to either take a photo of an item by placing it in the built-in photo booth, or uploading a file via a USB stick.<br><br>The team at INC Creative not only designed and built the 'Touchscreen App', but sourced and specified all hardware as well as commissioning the services of engineers to build the aluminium kiosks - all of which when combined provided some interesting challenges. Nonetheless, creative problem solving is what we do to provide yet another successful project for a happy client.
		    </div>


			<div class="pro-client" onClick="window.open('http://www.aucklandmuseum.com/')" style="margin-top:2.6%;">Client: <span>Auckland War Memorial Museum, New Zealand</span></div>

			<!-- <a class="pro-nav-btn" href="pna.php">previous project</a>
			<a class="pro-nav-btn" href="bbb.php" style="margin-left:120px;">next project</a> -->

		</div>
	</div> <!-- end of pro-wrapper -->
</div><!-- end of pro-page -->

<?php include_layout_template('mobile_footer.php') ?>