<?php
require_once('../../includes/init.php');
include_layout_template('mobile_header_2.php');
?>

<div id="bringingithome-page"> <!-- bringingithome page -->
	<div class="pro-wrapper">
		<div class="pro_title">XVI Stories<br>Interactive<br>kiosk<br>
		<!-- <div class="pro_sub_title">taranaki and<br>world war one</div> -->
		<div class="pro_category">Interactive touch screen Windows app</div>

		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/bbb_img_1_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/bbb_img_1.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/bbb_img_2_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/bbb_img_2.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/bbb_img_3_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/bbb_img_3.jpg" alt="image">
				</a>
			</li>
		</ul>
		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/bbb_img_4_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/bbb_img_4.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/bbb_img_5_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/bbb_img_5.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a class="swipebox" href="https://www.youtube.com/watch?v=7tVeEuovvdQ">
					<img class="thumb-img" style="position:absolute;" src="<?php echo PUBLIC_PATH ?>/images/play_icon.png">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/bbb_vid.jpg" alt="image">
				</a>
			</li>
		</ul>
		<div class="pro-description">
		      	Balls, Bullets & Boots, from rugby field to battlefield is a WW100 commemorative exhibition designed and produced by the New Zealand Rugby Museum, based in Palmerston North.<br><br>This captivating exhibition, set in the scene of a typical WW1 battlefield trench, uses rugby &#8220;as the common thread to link the stories of 100 years ago with people today ... features a wide array of rare rugby images, footage and displays - including dramatised short films and touchscreen interactives.&#8221;<br><br>Working alongside the very talented and motivated exhibition team at the New Zealand Rugby Museum, INC Creative was commissioned to design a &#8216;table top&#8217; touchscreen interactive in which two 55 inch touchscreens were fitted into custom-built tables. In addition to the touchscreen interactives, Balls, Bullets & Boots also features several audio-visual displays using motion-sensor activation - the videos start playing once a visitor is detected nearby. The team at INC also specified, developed and installed the touchscreens, audio -visual and motion-sensor equipment as well as editing and formatting the audio-visual content.<br><br>Company Managing Director, Lim Kai Teng, has called New Zealand home for over 17 years now and feels particularly proud to be involved in such a project. <br><br>&#8220;Rugby is part of our kiwi culture. The pride and honour associated with both playing our national game and serving our country during a war have been beautifully weaved into the Balls, Bullets & Boots exhibition. It allows the visitor to perhaps gain a better understanding of the tremendous effect both world wars had on everyday people's lives - their families, communities, our country as a whole - all those years ago.&#8221;
		    </div>


			<div class="pro-client" onClick="window.open('http://rugbymuseum.co.nz/')" style="margin-top:2.6%;">Client: <span>New Zealand Rugby Museum, New Zealand</span></div>

			<!-- <a class="pro-nav-btn" href="adu.php">previous project</a>
			<a class="pro-nav-btn" href="bringingithome.php" style="margin-left:120px;">next project</a> -->

		</div>
	</div> <!-- end of pro-wrapper -->
</div><!-- end of pro-page -->

<?php include_layout_template('mobile_footer.php') ?>