<?php
require_once('../../includes/init.php');
include_layout_template('mobile_header_2.php');
?>

<div id="samoa-page"> <!-- samoa page -->
	<div class="pro-wrapper">
		<div class="pro_title">samoa<br><div class="pro_sub_title">A Great<br>and Urgent<br>Imperial Service</div><div class="pro_category">Interactive touch screen Windows app</div>

		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/samoa_img_1_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/samoa_img_1.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/samoa_img_2_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/samoa_img_2.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/samoa_img_3_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/samoa_img_3.jpg" alt="image">
				</a>
			</li>
		</ul>
		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/samoa_img_4_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/samoa_img_4.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/samoa_img_5_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/samoa_img_5.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a class="swipebox" href="https://www.youtube.com/watch?v=pgP7PgZiecY">
					<img class="thumb-img" style="position:absolute;" src="<?php echo PUBLIC_PATH ?>/images/play_icon.png">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/samoa_vid.jpg" alt="image">
				</a>
			</li>
		</ul>
		<div class="pro-description">
		      	As part of the ongoing WWI centennial commemorations, the National Army Muesum recently opened an exhibition based on a little known historical event. The project 'Samoa' was about New Zealand forces relieving Samoa from German occupation and the events that took place thereafter.<br><br>Without many physical artefacts to display but hundreds of fully documented scanned images in the muesum's collection, our solution was to diplay this information in an easy access digital form. As the main control interface we used a 27&#34; touch screen. This was aided by a much larger wall mounted screen used to view the hi res images with zoom capabilities. A user would start by selecting an event from the timeline to explore further. Each event had its own set of images and narrated storyline. The images are randomly assorted in a pile, as if photgraphs on a table. The photographs could be rearranged by the user and/or inserted into the virtual projector;  instantly projecting the images on to the larger screen accompanied with a caption.
		    </div>


			<div class="pro-client" onClick="window.open('http://www.armymuseum.co.nz/')" style="margin-top:2.6%;">Client: <span>National Army Museum, Waiouru, New Zealand</span></div>

<!-- 			<a class="pro-nav-btn" href="kokako.php">previous project</a>
			<a class="pro-nav-btn" href="grommet.php" style="margin-left:120px;">next project</a> -->

		</div>
	</div> <!-- end of pro-wrapper -->
</div><!-- end of pro-page -->

<?php include_layout_template('mobile_footer.php') ?>