<?php
require_once('../../includes/init.php');
include_layout_template('mobile_header_2.php');
?>

<div id="bringingithome-page"> <!-- bringingithome page -->
	<div class="pro-wrapper">
		<div class="pro_title">E-OLOGY<br>ADVERTISING<br>SCREENS<br>
		<div class="pro_sub_title">65" TOUCHSCREEN KIOSKS,<br>75" DISPAY SCREEN DESIGN,<br>INSTALL & MAINTENANCE</div>
		<div class="pro_category">Advertising Screen</div>

		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/pna_img_1_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/pna_img_1.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/pna_img_2_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/pna_img_2.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/pna_img_3_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/pna_img_3.jpg" alt="image">
				</a>
			</li>
		</ul>
		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/pna_img_4_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/pna_img_4.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/pna_img_5_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/pna_img_5.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/pna_img_6_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/pna_img_6.jpg" alt="image">
				</a>
			</li>
		</ul>
		<div class="pro-description">
		      	Our sister company, E-ology is in the business of venue advertising. Having recently taken over the management of the advertising inside the terminal at Palmerston North Airport, E-ology relies on Inc's expertise to supply, design, develop and maintain all of its screen advertising platforms. From large display screens to touchscreen kiosks, Inc is kept busy continually developing everything from the 'Apps' that the screens use to display the advertising, through to a full backend media upload CMS and individual screen monitoring systems. Inc Creative will continue to push the boundaries of how advertisers will engage with their audience... or what E-ology calls 'Engagement - technology.'
		    </div>


			<div class="pro-client" onClick="window.open('http://www.pnairport.co.nz/')" style="margin-top:2.6%;">Client: <span>Palmerston North Airport Limited, New Zealand</span></div>

			<div class="pro-client" onClick="window.open('http://www.e-ology.co.nz/')">Partner: <span>E-ology Limited</span></div>

			<a class="pro-nav-btn" href="hyundai_kiosk.php">previous project</a>
			<a class="pro-nav-btn" href="adu.php" style="margin-left:120px;">next project</a>

		</div>
	</div> <!-- end of pro-wrapper -->
</div><!-- end of pro-page -->

<?php include_layout_template('mobile_footer.php') ?>