<?php
require_once('../../includes/init.php');
include_layout_template('mobile_header_2.php');
?>

	<div class="quick-facts-wrapper">
		<div class="quick-facts-title">
			<h7>WE DO JUST ABOUT ANYTHING. BUT WE'RE EXPERTS IN<span>_</span><br>INTERACTIVE DESIGN.</h7>
		</div>
		<div class="client_text">Clients we have worked with: </div>

		<ul class="logo_wrapper">

			<li><img class="client_logo" src="<?php echo PUBLIC_PATH ?>/images/client_logos/aa.jpg"></li>
			<li><img class="client_logo" src="<?php echo PUBLIC_PATH ?>/images/client_logos/am.jpg"></li>
			<li><img class="client_logo" src="<?php echo PUBLIC_PATH ?>/images/client_logos/anz-large.jpg"></li>
			<!-- <li><img class="client_logo" src="<?php echo PUBLIC_PATH ?>/images/client_logos/anz-netball-championships.jpg"></li> -->
			<li><img class="client_logo" src="<?php echo PUBLIC_PATH ?>/images/client_logos/ASB_Bank_logo_(2004-2012).jpg"></li>
			<li><img class="client_logo" src="<?php echo PUBLIC_PATH ?>/images/client_logos/flyPalmy.jpg"></li>
			<li><img class="client_logo" src="<?php echo PUBLIC_PATH ?>/images/client_logos/fonterra.jpg"></li>
			<li><img class="client_logo" src="<?php echo PUBLIC_PATH ?>/images/client_logos/hyundai-logo.jpg"></li>
			<li><img class="client_logo" src="<?php echo PUBLIC_PATH ?>/images/client_logos/NAM_logo.jpg"></li>
			<li><img class="client_logo" src="<?php echo PUBLIC_PATH ?>/images/client_logos/NZFS.jpg"></li>
			<li><img class="client_logo" src="<?php echo PUBLIC_PATH ?>/images/client_logos/nzrm.jpg"></li>
			<li><img class="client_logo" src="<?php echo PUBLIC_PATH ?>/images/client_logos/Olex.jpg"></li>
			<!-- <li><img class="client_logo" src="<?php echo PUBLIC_PATH ?>/images/client_logos/Orange_Group_Logo.jpg"></li> -->
			<li><img class="client_logo" src="<?php echo PUBLIC_PATH ?>/images/client_logos/Ports_of_AKL.jpg"></li>
			<li><img class="client_logo" src="<?php echo PUBLIC_PATH ?>/images/client_logos/puke-ariki.jpg"></li>
			<li><img class="client_logo" src="<?php echo PUBLIC_PATH ?>/images/client_logos/RWC2011.jpg"></li>
			<li><img class="client_logo" src="<?php echo PUBLIC_PATH ?>/images/client_logos/samsung-logo.jpg"></li>
			<li><img class="client_logo" src="<?php echo PUBLIC_PATH ?>/images/client_logos/screenVista.jpg"></li>
			<!-- <li><img class="client_logo" src="<?php echo PUBLIC_PATH ?>/images/client_logos/TAFT_logoMono.jpg"></li> -->
			<!-- <li><img class="client_logo" src="<?php echo PUBLIC_PATH ?>/images/client_logos/temanawa.jpg"></li> -->
			<li><img class="client_logo" src="<?php echo PUBLIC_PATH ?>/images/client_logos/te-papa-logo.jpg"></li>
			<li><img class="client_logo" src="<?php echo PUBLIC_PATH ?>/images/client_logos/techDome_Penang.jpg"></li>
			<li><img class="client_logo" src="<?php echo PUBLIC_PATH ?>/images/client_logos/teppapa-dpc.jpg"></li>
			<!-- ======================== NEWLY ADDED AT 2019 OCT 23 ========================== -->
			<li><img class="client_logo" src="../images/client_logos/suzuki-logo.jpg"></li>
			<li><img class="client_logo" src="../images/client_logos/npdc-logo.jpg"></li>
			<li><img class="client_logo" src="../images/client_logos/unoloco-logo.jpg"></li>
			<li><img class="client_logo" src="../images/client_logos/99-logo.jpg"></li>
			<li><img class="client_logo" src="../images/client_logos/pncc-logo.jpg"></li>
		</ul>

		<div class="quick_fact_contact">
			CONTACT US NOW : <br>
<!-- 			<div style="padding-top:10px;">MARTIN<br><a href="skype:inccreative">+60 17 7272388</a>  / <br><a href="mailto:martin@inccreative.asia">martin@inccreative.asia</a></div> -->
			<div><br>LIM<br><a href="skype:inccreative">+64 21 1407607</a>  / <br><a href="mailto:lim@inccreative.asia">lim@inccreative.co.nz</a></div>
		</div>
	</div>



<!-- contact -->
<?php include_layout_template('mobile_footer.php') ?>
