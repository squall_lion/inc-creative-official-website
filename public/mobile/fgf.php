<?php
require_once('../../includes/init.php');
include_layout_template('mobile_header_2.php');

$title_name = "fgf";
$vid_id_1 = "mB8aFuwZJHE";
$vid_id_2 = "g4YEaOhfKjc";
?>

<div id="bringingithome-page"> <!-- bringingithome page -->
	<div class="pro-wrapper">
		<div class="pro_title">Food Glorious Food
		<div class="pro_sub_title">An army marches on its stomach</div>
		<div class="pro_category">INTERACTIVE & GAME</div> 

		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/<?php echo $title_name;?>_img_1_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/<?php echo $title_name;?>_img_1.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/<?php echo $title_name;?>_img_2_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/<?php echo $title_name;?>_img_2.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/<?php echo $title_name;?>_img_3_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/<?php echo $title_name;?>_img_3.jpg" alt="image">
				</a>
			</li>
		</ul>
		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/<?php echo $title_name;?>_img_4_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/<?php echo $title_name;?>_img_4.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a class="swipebox" href="https://www.youtube.com/watch?v=<?php echo $vid_id_1;?>">
					<img class="thumb-img" style="position:absolute;" src="<?php echo PUBLIC_PATH ?>/images/play_icon.png">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/<?php echo $title_name;?>_vid_1.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a class="swipebox" href="https://www.youtube.com/watch?v=<?php echo $vid_id_2;?>">
					<img class="thumb-img" style="position:absolute;" src="<?php echo PUBLIC_PATH ?>/images/play_icon.png">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/<?php echo $title_name;?>_vid_2.jpg" alt="image">
				</a>
				<!-- <a onClick="window.open('http://tvnz.co.nz/national-news/auckland-war-memorial-museum-looks-digital-legacy-video-6307736')">
					<img class="thumb-img" style="position:absolute;" src="<?php echo PUBLIC_PATH ?>/images/play_icon.png">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/adu_vid.jpg" alt="image">
				</a> -->
			</li>
		</ul>
		<div class="pro-description">
						The National Army Museum Te Mata Toa in Waiouru has a long-standing relationship with Inc Creative when looking for innovative and engaging interactive exhibition content. Their latest exhibition, Food glorious food, takes the visitor away from the trenches and into the cook houses of the New Zealand Division during World War One. 
						<br><br>
						The 'Moments in Time' interactive takes the user back in time to the WW1 era to show the kinds of facilities, food and ingredients that were available during a period where modern-day commodities were scarce.
						<br><br>
						Featuring several interactive sections that will appeal to all ages and levels of interest: From a series of photo walls focusing on historic footage of recreational facilities such as the Soldiers Clubs, the Y.M.C.A and the bars & canteens serving alcohol, to a section showing recipes in a book which also features a touchscreen game presented by the character 'Corporal Ratty' - where the user 'catches' the ingredients of Hokey Pokey biscuits in a bowl that a rat knocks off the shelf when it runs past.
						<br><br>
						In addition to the design, development and building of the interactive application, Inc Creative also specified and supplied the 55 inch 'Interactive flat panel' touchscreen that was fitted into a table custom-built by the National Army Museum to compliment the exhibition space.
						<br><br>
						Food glorious food is a must see for all you foodies out there next time you're in Waiouru.
		    </div>


			<!-- <div class="pro-client" onClick="window.open('http://www.aucklandmuseum.com/')" style="margin-top:2.6%;">Client: <span>Auckland War Memorial Museum, New Zealand</span></div> -->
			<div class="pro-client" onClick="window.open('http://www.armymuseum.co.nz/')" style="margin-top:2.6%;">Client: <span>National Army Museum, Waiouru, New Zealand</span></div>

			<!-- <a class="pro-nav-btn" href="pna.php">previous project</a> -->
			<!-- <a class="pro-nav-btn" href="bbb.php" style="margin-left:120px;">next project</a> -->

		</div>
	</div> <!-- end of pro-wrapper -->
</div><!-- end of pro-page -->

<?php include_layout_template('mobile_footer.php') ?>