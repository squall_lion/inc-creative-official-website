<?php
require_once('../../includes/init.php');
include_layout_template('mobile_header_2.php');
?>

<div id="grommet-page"> <!-- grommet page -->

	<div class="partnership_wrapper noselect">
		<div class="nd_title" style="min-width:225px;"><h4>japan<br>black<span>_</span></h4></div>

		<div class="archive_projects_wrapper" style="margin-top:30px;" onclick="location.href='black-exhibition'">
			<div class="archive_projects_title center_align">Japan<br>Black<span>_</span> <br>exhibition</div>
		</div>

		<div class="archive_projects_wrapper" style="margin-top:30px;" onclick="location.href='black-interactive'">
			<div class="archive_projects_title center_align">Japan<br>Black<span>_</span> <br>interactive</div>
		</div>

		<div class="archive_projects_wrapper" onclick="location.href='black-photobooth'">
			<div class="archive_projects_title center_align">Japan<br>Black<span>_</span> <br>photobooth</div>
		</div>

	</div>
</div>

<!-- contact -->
<?php include_layout_template('mobile_footer.php') ?>
