<?php
require_once('../../includes/init.php');
include_layout_template('mobile_header_2.php');
?>
<div id="specialty-page">
	<div class="specialty-wrapper">
		<h4>specialty</h4><br><br>
		<h6><span>Digital solution - Research, development and delivery</span><br><br>Interactive design and development<br>Exhibition design<br>Haptic interactive experience<br>Kinect, Leap and Unity developer<br><br>Interactive game development<br>Augmented Reality (AR) and QR code<br>Mobile APP development - Android, iOS and Windows<br>RFID tracking and identification<br>Digital Signage software and hardware solution<br><br><br><span>Hardware :</span><br>Touch screen kiosk (from 5" to 110")<br>Interactive Flat Panel (IFP, from 20" to 110")<br>Rear projection foil<br>LED screen<br><br><br><span>Digital Graphics :</span><br>Branding, and all printed materials<br>Video production and post-production<br>Motion Graphics</h6>
	</div>
</div>
<!-- contact -->
<?php include_layout_template('mobile_footer.php') ?>