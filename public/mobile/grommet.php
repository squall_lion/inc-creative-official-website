<?php
require_once('../../includes/init.php');
include_layout_template('mobile_header_2.php');
?>

<div id="grommet-page"> <!-- grommet page -->
	<div class="pro-wrapper">
		<div class="pro_title">grommet<br><div class="pro_category">Kinect based surfing game</div>

		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/gro_img_1_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/gro_img_1.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/gro_img_2_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/gro_img_2.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/gro_img_3_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/gro_img_3.jpg" alt="image">
				</a>
			</li>
		</ul>
		<ul id="thumb-row">
			<li class="pro-thumb-list">
				<a href="<?php echo PUBLIC_PATH ?>/images/gro_img_4_large.jpg" class="swipebox">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/gro_img_4.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a class="swipebox" href="http://www.youtube.com/watch?v=Ah9iObnU510">
					<img class="thumb-img" style="position:absolute;" src="<?php echo PUBLIC_PATH ?>/images/play_icon.png">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/grommet_vid_rocky.jpg" alt="image">
				</a>
			</li>
			<li class="pro-thumb-list">
				<a class="swipebox" href="http://www.youtube.com/watch?v=fEOkeD3Fv2U">
					<img class="thumb-img" style="position:absolute;" src="<?php echo PUBLIC_PATH ?>/images/play_icon.png">
					<img class="thumb-img" src="<?php echo PUBLIC_PATH ?>/images/grommet_vid_bogworks.jpg" alt="image">
				</a>
			</li>
		</ul>
		<div class="pro-description">
		      	Grommet is a highly immersive surfing game built for Puke Ariki's 'Surf' exhibition. It uses Microsoft's Kinect camera to detect the players' body movements allowing them to control the surfboard extremely accurately and in turn surf in their own style and stance.<br><br>There are two breaks to choose from, both with their own wave breaks and locations; Rocky Point and as the locals known as Bogworks. To simulate the locations, a local photographer used a Jetski to get out to the breaks and took panoramic photos which we then used to translate in the 3D game software, Unity3D.<br><br>Playing the game users will find that it is not particularly easy to surf, at first. To coincide with actual surfing, the player needs to practise basic manoeuvres before attempting the more advanced tricks. To encourage the player to attempt and learn these tricks we added an achievement system with up to 10 to gain. The achievements are calculated and added to the players' final score giving them a potential edge over other players in a local high score system. The player is then given a current ranking and put up against 'Todays Rank' and 'All Time Rank' to see how well they did in comparison with other players.
		    </div>


			<div class="pro-client" onClick="window.open('http://www.pukeariki.com/')" style="margin-top:2.6%;">Client: <span>Puke Ariki Museum, New Zealand</span></div>

			<!-- <a class="pro-nav-btn" href="bringingithome.php">previous project</a>
			<a class="pro-nav-btn" href="hyundai_kiosk.php" style="margin-left:120px;">next project</a> -->

		</div>
	</div> <!-- end of pro-wrapper -->
</div><!-- end of pro-page -->

<?php include_layout_template('mobile_footer.php') ?>