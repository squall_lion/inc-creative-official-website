<?php
require_once('../includes/init.php');
include_layout_template('header.php');
?>

	<div id="nd_content_wrapper" class="noselect">

				<div id="nd_spec_title">
					<h2>WE ARE<br>EXPANDING OUR REACH<span>_</span></h2>
				</div>

				<div class="nd_description">
					<!-- In addition to working with  <a class="nd_btn" href="http://www.orangeproductions.co.nz/" target="_blank">The Orange Group</a> for many years, we are also affiliated with <a class="nd_btn" href="http://www.brandstand.co.nz/" target="_blank">Brandstand</a> and <a class="nd_btn" href="http://www.e-ology.co.nz" target="_blank">E-ology</a>. -->
					In addition to working with the <a class="nd_btn" href="http://www.orangeproductions.co.nz/" target="_blank">The Orange Group</a> for many years, we are also affiliated with <a class="nd_btn" href="http://www.brandstand.co.nz/" target="_blank">Brandstand</a> and <a class="nd_btn" href="http://www.e-ology.co.nz" target="_blank">e-ology</a> and have invested in a new partnership with <span class="nd_btn">IES &#8211; Interactive Exhibition Specialists</span>.
				</div>

				<div class="nd_logo_wrapper">
					<img  onclick="window.open('http://www.e-ology.co.nz')" src="images/logo-eology.png" style="cursor:pointer;">
						<div class="nd_logo_title">e-ology <span>X</span> INC Creative
							<div class="nd_logo_description">Our sister company, E-ology is in the business of venue advertising. Having recently taken over the management of the advertising inside the terminal at Palmerston North Airport, E-ology relies on Inc's expertise to supply, design, develop and maintain all of its screen advertising media & platforms.</div>
						</div>
				</div>

				<div class="nd_logo_wrapper">
					<img  onclick="window.open('http://www.brandstand.co.nz/')" src="images/logo-brandstand.png" style="cursor:pointer;">
						<div class="nd_logo_title">Brandstand
							<div class="nd_logo_description">A market leader of display and exhibition stands in NZ for over 16 years, Brandstand provides creative exhibition products like no other. By incorporating new technologies into the latest design, Inc Creative is proud to be a new-media partner with Brandstand to provide our clients with cutting-edge solutions that 'stop traffic'.</div>
						</div>
				</div>

				<div class="nd_logo_wrapper">
					<img  onclick="window.open('http://iexhibitions.co.nz/')" src="images/logo-ies.png" style="cursor:pointer;">
						<div class="nd_logo_title">Interactive Exhibition Specialists
							<div class="nd_logo_description">Just as the name suggests, getting visitors engaged with hands-on immersive exhibition content is what this organisation serving the museums and science centre of the Asia-Pacific region is all about.  Staffed by a team of experienced concept developers, designers, fabricators and project managers who have worked across the spectrum of science centre and museum visitor experiences, Inc Creative is excited to be a founding partner in this new initiative. </div>
						</div>
				</div>

				<div class="nd_logo_wrapper">
					<img  onclick="window.open('https://www.displays2go.com.au')" src="images/partnership_d2g_logo.jpg" style="cursor:pointer;">
						<div class="nd_logo_title">Displays 2 Go
							<div class="nd_logo_description">Displays 2 Go is based in Sydney Australia, part of Brandstand NZ group.</div>
						</div>
				</div>

	</div>


<?php include_layout_template('fancy_box_popup.php') ?>

<?php include_layout_template('footer.php') ?>
