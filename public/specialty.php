<?php
require_once('../includes/init.php');
include_layout_template('header.php');
?>

<div id="spec_content_wrapper" class="noselect">
			<div id="spec_title">
				<h2>WE DO JUST ABOUT ANYTHING.<br>BUT WE'RE EXPERTS IN<span>_</span><br> INTERACTIVE DESIGN.</h2>
				<a id="myButton" href="#"><img class="more_btn" src="images/more_btn.png"
			onmouseover="this.src='images/more_btn_hover.png';"
			onmouseout="this.src='images/more_btn.png';" alt="More Details"></a>

			

			</div>	

</div>

					<!-- this block below is in the popup.css -->

		<div id="Popup">
		      <div class="desk_spec_description">
		      	<span>Digital solution - Research, development and delivery</span><br><br>Interactive design and development<br>Exhibition design<br>Haptic interactive experience<br>Kinect, Leap and Unity developer<br><br>Interactive game development<br>Augmented Reality (AR) and QR code<br>Mobile APP development - Android, iOS and Windows<br>RFID tracking and identification<br>Digital Signage software and hardware solution<br><br><br><span>Hardware :</span><br>Touch screen kiosk (from 5" to 110")<br>Interactive Flat Panel (IFP, from 20" to 110")<br>Rear projection foil<br>LED screen<br><br><br><span>Digital Graphics :</span><br>Branding, and all printed materials<br>Video production and post-production<br>Motion Graphics

		      </div>

		      <div class="popupClose_wrapper"><a id="popupClose"><img class="popup_close_btn" src="images/popup_close.png"
					onmouseover="this.src='images/popup_close_hover.png';"
					onmouseout="this.src='images/popup_close.png';"></a>
			  </div>  
		  </div>  <!-- popup ends here -->
		<div id="bgPopup"></div> 

		<!-- this block above is in the popup.css -->

<?php include_layout_template('fancy_box_popup.php') ?>

<?php include_layout_template('footer.php') ?>