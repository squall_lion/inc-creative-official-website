<?php
require_once('../includes/init.php');
include_layout_template('header.php');
?>


<div id="pro_wrapper" class="noselect" style="z-index:1;">
				<div class="pro_title">grommet</div>
				<div class="pro_category">Kinect based surfing game</div> 
				

<div class="pro_description">
	Grommet is a highly immersive surfing game built for Puke Ariki's 'Surf' exhibition. It uses Microsoft's Kinect camera to detect the players body movements allowing them to control the surfboard extremely accurately and in turn surf in their own style and stance...<a id="myButton" href="#"><span> READ MORE</span></a>
</div>
<div class="pro_client" onClick="window.open('http://www.pukeariki.com/')" style="margin-top:2.6%;">Client: <span>Puke Ariki Museum, New Plymouth, New Zealand</span></div>

<div class="thumb_wrapper">
		<a class="fancybox noselect" href="images/gro_img_1_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/gro_img_1.jpg" alt="" style="margin-left:0;" /></a>
		<!-- <a class="fancybox" href="images/gro_img_2_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/gro_img_2.jpg" alt="" /></a> -->
		<a class="fancybox noselect" href="images/gro_img_3_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/gro_img_3.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/gro_img_4_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/gro_img_4.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/gro_img_5_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/gro_img_5.jpg" alt="" /></a>
		<a class="fancybox fancybox.iframe noselect" href="http://www.youtube.com/embed/Ah9iObnU510" data-fancybox-group="gallery"><img class="pro_thumb" style="background:url(images/grommet_vid_rocky.jpg) no-repeat; background-size: cover;" src="images/play_icon.png" alt=""/></a> 
		<a class="fancybox fancybox.iframe noselect" href="http://www.youtube.com/embed/fEOkeD3Fv2U" data-fancybox-group="gallery"><img class="pro_thumb" style="background:url(images/grommet_vid_bogworks.jpg) no-repeat; background-size: cover;" src="images/play_icon.png" alt=""/></a> 
	</div> <!-- thumb_wrapper -->
</div> <!-- pro_wrapper -->


	<!-- this block below is in the popup.css -->

		<div id="Popup">
		      <div class="pro_description" style="margin-left:auto; margin-right:auto; max-width:600px;">
		      	Grommet is a highly immersive surfing game built for Puke Ariki's 'Surf' exhibition. It uses Microsoft's Kinect camera to detect the players' body movements allowing them to control the surfboard extremely accurately and in turn surf in their own style and stance.<br><br>There are two breaks to choose from, both with their own wave breaks and locations; Rocky Point and as the locals known as Bogworks. To simulate the locations, a local photographer used a Jetski to get out to the breaks and took panoramic photos which we then used to translate in the 3D game software, Unity3D.<br><br>Playing the game users will find that it is not particularly easy to surf, at first. To coincide with actual surfing, the player needs to practise basic manoeuvres before attempting the more advanced tricks. To encourage the player to attempt and learn these tricks we added an achievement system with up to 10 to gain. The achievements are calculated and added to the players' final score giving them a potential edge over other players in a local high score system. The player is then given a current ranking and put up against 'Todays Rank' and 'All Time Rank' to see how well they did in comparison with other players.
		      </div>

		      <div class="popupClose_wrapper"><a id="popupClose"><img class="popup_close_btn" src="images/popup_close.png"
					onmouseover="this.src='images/popup_close_hover.png';"
					onmouseout="this.src='images/popup_close.png';"></a>
			  </div>  
		  </div>  <!-- popup ends here -->
		<div id="bgPopup"></div> 

		<!-- this block above is in the popup.css -->


<video autoplay loop poster="vid/grommet_bg_vid.jpg" id="bgvid">
<source src="vid/grommet_bg_vid.webm" type="video/webm">
<source src="vid/grommet_bg_vid.mp4" type="video/mp4">
</video>


<?php include_layout_template('fancy_box_popup.php') ?>

<?php include_layout_template('footer.php') ?>