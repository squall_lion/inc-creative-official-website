<?php
require_once('../includes/init.php');
include_layout_template('header.php');

$project_title="Boxthorn Cutter Animation";
$project_subtitle="";
$project_category="2D Animation";

$project_client="PUKE ARIKI MUSEUM, NEW PLYMOUTH, NEW ZEALAND";
$project_client_url="http://pukeariki.com/";

$projectName_img = "boxthorn";
$project_YouTube_link = "KpIKOoEdZU0";

$project_description_short="Bringing history alive is one of our specialities and our friends at Puke Ariki in New Plymouth often inspire us to try new approaches in conveying stories to their visitors...";

$project_description_full="From a chalk drawing on his engineering shed floor to a ‘cutting-hedge design’, Lou Butler’s innovative 1940s boxthorn cutter invention takes pride of place in a new Puke Ariki gallery space. Projected alongside it on the wall is a large format animation developed by Inc Creative to tell the story of how Lou came up with the idea for this giant, mechanical, labour-saving device.
<br><br>
Using  a mix of sepia-toned animated artwork and real footage, the 4 minute animation with jaunty soundtrack was designed in response to the client’s brief to explain the origins of Lou’s invention in a relaxed and humorous way (look out for the bunny!). 
<br><br>
It’s a great tale of kiwi ingenuity and just one of many this Taranaki inventor tinkered into life. Read about them all <a style='color:white;text-decoration: underline;' href='http://pukeariki.com/Learning-Research/Taranaki-Research-Centre/Taranaki-Stories/Taranaki-Story/id/522/title/lou-butlers-boxthorn-battle' target='_blank'>here</a>.";

?>


<div id="pro_wrapper" class="noselect" style="z-index:1;">
				<div class="pro_title"><?php echo $project_title; ?><br>
				<!-- <div class="pro_sub_title"><?php echo $project_subtitle; ?></div> -->
				</div>
				<div class="pro_category"><?php echo $project_category; ?></div> 
				

<div class="pro_description"><?php 	echo $project_description_short; ?><a id="myButton" href="#"><span> READ MORE</span></a>
</div>
<div class="pro_client" onClick="window.open('<?php echo $project_client_url; ?>')" style="margin-top:2.6%;">Client: <span><?php echo $project_client ?></span></div>

<div class="thumb_wrapper noselect">
		<a class="fancybox noselect" href="images/<?php echo $projectName_img; ?>_img_1_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/<?php echo $projectName_img; ?>_img_1.jpg" alt="" style="margin-left:0;" /></a>
		<a class="fancybox noselect" href="images/<?php echo $projectName_img; ?>_img_2_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/<?php echo $projectName_img; ?>_img_2.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/<?php echo $projectName_img; ?>_img_3_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/<?php echo $projectName_img; ?>_img_3.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/<?php echo $projectName_img; ?>_img_4_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/<?php echo $projectName_img; ?>_img_4.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/<?php echo $projectName_img; ?>_img_5_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/<?php echo $projectName_img; ?>_img_5.jpg" alt="" /></a>
		<a class="fancybox fancybox.iframe noselect" href="http://www.youtube.com/embed/<?php echo $project_YouTube_link;?>" data-fancybox-group="gallery"><img class="pro_thumb" style="background:url(images/<?php echo $projectName_img; ?>_img_6.jpg) no-repeat; background-size: cover;" src="images/play_icon.png" alt=""/></a> 
		<!-- <a class="fancybox noselect" href="images/adu_img_1_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/adu_img_1.jpg" alt="" /></a> -->
	</div> <!-- thumb_wrapper -->
</div> <!-- pro_wrapper -->


	<!-- this block below is in the popup.css -->

		<div id="Popup">
		      <div class="pro_description" style="margin-left:auto; margin-right:auto; max-width:600px;">
		      	<?php 	echo $project_description_full; ?>
		      </div>

		      <div class="popupClose_wrapper"><a id="popupClose"><img class="popup_close_btn" src="images/popup_close.png"
					onmouseover="this.src='images/popup_close_hover.png';"
					onmouseout="this.src='images/popup_close.png';"></a>
			  </div>  
		  </div>  <!-- popup ends here -->
		<div id="bgPopup"></div> 

		<!-- this block above is in the popup.css -->


<video autoplay loop poster="vid/<?php echo $projectName_img; ?>_bg_vid.jpg" id="bgvid">
<source src="vid/<?php echo $projectName_img; ?>_bg_vid.webm" type="video/webm">
<source src="vid/<?php echo $projectName_img; ?>_bg_vid.mp4" type="video/mp4">
</video>


<?php include_layout_template('fancy_box_popup.php') ?>

<?php include_layout_template('footer.php') ?>