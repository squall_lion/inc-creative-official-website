<?php
require_once('../includes/init.php');
include_layout_template('header.php');

$project_title="Taranaki Tales";
$project_subtitle="";
$project_category="Windows Universal App";

$project_client="PUKE ARIKI MUSEUM, NEW PLYMOUTH, NEW ZEALAND";
$project_client_url="http://pukeariki.com/";

$projectName_img = "tt";
$project_YouTube_link = "3_6vqK9adM0";

$project_description_short="A new Inc Creative-designed touchscreen exhibit telling the stories of the region’s storytellers features in a new permanent gallery space at Puke Ariki...";

$project_description_full="The client wanted an intimate one-to-one digital experience that made the visitor feel a little like they were peeking into a writer’s notebook.  We developed a 12” touchscreen kiosk interface and accompanying content management system that allows the museum’s team to profile the lives of local writers and storytellers in their gallery space with ease. The simple interface allows Puke Ariki’s archival audio-visual and collection material to shine through, layering up the tales of these regional literati and their connections to the region and allowing visitors to dip into what takes their fancy. 
<br><br>
Ideal for smaller museum spaces, this web-based exhibit can be easily added to or content changed by the client and can run offline (after a daily, early morning ‘check-in’ to update content files";

?>


<div id="pro_wrapper" class="noselect" style="z-index:1;">
				<div class="pro_title"><?php echo $project_title; ?><br>
				<!-- <div class="pro_sub_title"><?php echo $project_subtitle; ?></div> -->
				</div>
				<div class="pro_category"><?php echo $project_category; ?></div> 
				

<div class="pro_description"><?php 	echo $project_description_short; ?><a id="myButton" href="#"><span> READ MORE</span></a>
</div>
<div class="pro_client" onClick="window.open('<?php echo $project_client_url; ?>')" style="margin-top:2.6%;">Client: <span><?php echo $project_client ?></span></div>

<div class="thumb_wrapper noselect">
		<a class="fancybox noselect" href="images/<?php echo $projectName_img; ?>_img_1_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/<?php echo $projectName_img; ?>_img_1.jpg" alt="" style="margin-left:0;" /></a>
		<a class="fancybox noselect" href="images/<?php echo $projectName_img; ?>_img_2_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/<?php echo $projectName_img; ?>_img_2.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/<?php echo $projectName_img; ?>_img_3_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/<?php echo $projectName_img; ?>_img_3.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/<?php echo $projectName_img; ?>_img_4_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/<?php echo $projectName_img; ?>_img_4.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/<?php echo $projectName_img; ?>_img_5_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/<?php echo $projectName_img; ?>_img_5.jpg" alt="" /></a>
		<a class="fancybox fancybox.iframe noselect" href="http://www.youtube.com/embed/<?php echo $project_YouTube_link;?>" data-fancybox-group="gallery"><img class="pro_thumb" style="background:url(images/<?php echo $projectName_img; ?>_vid.jpg) no-repeat; background-size: cover;" src="images/play_icon.png" alt=""/></a> 
		<!-- <a class="fancybox noselect" href="images/adu_img_1_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/adu_img_1.jpg" alt="" /></a> -->
	</div> <!-- thumb_wrapper -->
</div> <!-- pro_wrapper -->


	<!-- this block below is in the popup.css -->

		<div id="Popup">
		      <div class="pro_description" style="margin-left:auto; margin-right:auto; max-width:600px;">
		      	<?php 	echo $project_description_full; ?>
		      </div>

		      <div class="popupClose_wrapper"><a id="popupClose"><img class="popup_close_btn" src="images/popup_close.png"
					onmouseover="this.src='images/popup_close_hover.png';"
					onmouseout="this.src='images/popup_close.png';"></a>
			  </div>  
		  </div>  <!-- popup ends here -->
		<div id="bgPopup"></div> 

		<!-- this block above is in the popup.css -->


<video autoplay loop poster="vid/<?php echo $projectName_img; ?>_bg_vid.jpg" id="bgvid">
<source src="vid/<?php echo $projectName_img; ?>_bg_vid.webm" type="video/webm">
<source src="vid/<?php echo $projectName_img; ?>_bg_vid.mp4" type="video/mp4">
</video>


<?php include_layout_template('fancy_box_popup.php') ?>

<?php include_layout_template('footer.php') ?>