<?php
require_once('../includes/init.php');
include_layout_template('header.php');
?>

<div id="pro_wrapper" class="noselect" style="z-index:1;">
				<div class="pro_title">#instaBUGS
				<div class="pro_sub_title">CAMOUFLAGE YOURSELF INTERACTIVE</div>
				</div>
				<div class="pro_category">Interactive Kiosk</div> 
				

<div class="pro_description">
#instaBUGS is yet another fun and engaging interactive thanks to our long-standing partnership with Puke Ariki for the 'BUGS! Our backyard heroes' exhibit...<a id="myButton" href="#"><span> READ MORE</span></a>
</div>
<div class="pro_client" onClick="window.open('http://pukeariki.com/')" style="margin-top:2.6%;">Client: <span>PUKE ARIKI MUSEUM, NEW PLYMOUTH, NEW ZEALAND</span></div>
<br>
<!-- <div class="pro_client" onClick="window.open('http://www.sciencealive.co.nz/')">IN ASSOCIATION WITH: <span>SCIENCE ALIVE, CHRISTCHURCH, New Zealand</span></div>
 -->
<div class="thumb_wrapper noselect">
		<a class="fancybox noselect" href="images/instabugs_img_1_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/instabugs_img_1.jpg" alt="" style="margin-left:0;" /></a>
		<a class="fancybox noselect" href="images/instabugs_img_2_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/instabugs_img_2.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/instabugs_img_3_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/instabugs_img_3.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/instabugs_img_4_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/instabugs_img_4.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/instabugs_img_5_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/instabugs_img_5.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/instabugs_img_6_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/instabugs_img_6.jpg" alt="" /></a>
		<!-- <a class="fancybox fancybox.iframe noselect" href="http://www.thestar.com.my/videos/2016/04/29/defy-newton-law-of-motion-at-tech-dome-penang/" data-fancybox-group="gallery"><img class="pro_thumb" src="images/skeleton_img_6.jpg" alt=""/></a>  --> 
		<!-- <a class="fancybox fancybox.iframe noselect" href="http://tvnz.co.nz/national-news/auckland-war-memorial-museum-looks-digital-legacy-video-6307736" data-fancybox-group="gallery"><img class="pro_thumb" style="background:url(images/adu_vid.jpg) no-repeat; background-size: cover;" src="images/play_icon.png" alt=""/></a>  -->
	</div> <!-- thumb_wrapper -->
</div> <!-- pro_wrapper -->


	<!-- this block below is in the popup.css -->

		<div id="Popup">
			<!-- <div class="longContent" style="overflow:hidden;">  -->
			      <div class="pro_description" style="margin-left:auto; margin-right:auto; max-width:600px;">    	
						Designed to appeal to a younger audience, this educational and vibrantly colourful touch screen interactive features the camouflage defence mechanisms of 3 bugs found in NZ: The praying mantis, the stick insect and the bird-dropping spider. 
						<br><br>
						The user is invited to ‘camouflage yourself’ in an on-screen simulation similar to the camouflage appearance of the chosen bug. You can then take a ‘3-2-1, snapshot’ of your camouflaged-self and share the image on a larger screen nearby and/or by email as a mechanism to also promote the exhibit to a wider audience.
						<br><br>
						As well as being educational, the interactivity has a fun aspect with users able to wear supplied ‘props’ that match the green screen behind them to further enhance the camouflage effect reinforcing the idea that some bugs can change their appearance to blend in better with their surroundings. 
						<br><br>
						The team at Inc Creative worked closely with Puke Ariki to develop an interactive that fulfilled the brief within the budget and timeframe. An exciting element in this project is the utilisation of the 55 inch Interactive Flat Panel (IFP) which is becoming increasingly popular for anyone wanting an ‘all-in-one’ touchscreen solution.
						<br><br>
						“Our 55" IFP’s are the sister to our free-standing kiosks that are ins use throughout NZ, but obviously more portable and flexible in terms of their ability to display in either landscape or portrait.” Comments Kai Teng Lim, Managing Director, Inc Creative Limited.
						<br><br>
						Whilst specialising in Interactive Design, Inc Creative also imports many touchscreen hardware configurations at very competitive prices.
		     <!--  </div> -->
	      </div>

		      <div class="popupClose_wrapper"><a id="popupClose"><img class="popup_close_btn" src="images/popup_close.png"
					onmouseover="this.src='images/popup_close_hover.png';"
					onmouseout="this.src='images/popup_close.png';"></a>
			  </div>  
		  </div>  <!-- popup ends here -->
		<div id="bgPopup"></div> 

		<!-- this block above is in the popup.css -->


<video autoplay loop poster="vid/instabugs_bg_vid.jpg" id="bgvid">
<source src="vid/instabugs_bg_vid.webm" type="video/webm">
<source src="vid/instabugs_bg_vid.mp4" type="video/mp4">
</video>




<?php include_layout_template('fancy_box_popup.php') ?>

<?php include_layout_template('footer.php') ?>