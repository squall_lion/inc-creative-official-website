<?php
require_once('../includes/init.php');
include_layout_template('header.php');
?>


<div id="pro_wrapper" class="noselect" style="z-index:1;">
				<div class="pro_title">Skeleton Moves<br>
				<div class="pro_sub_title">KINECT MOTION SENSOR CAMERA<br>INTERACTIVE DISPLAYING ON A 65&#34; SCREEN DESIGN & INSTALL.</div>
				</div>
				<div class="pro_category">Interactive game</div> 
				

<div class="pro_description">
Our brief was to design & develop an interactive aimed at a younger audience, allowing them to gain an understanding of how a human skeleton moves, and the connection between the bones....<a id="myButton" href="#"><span> READ MORE</span></a>
</div>
<div class="pro_client" onClick="window.open('http://techdomepenang.org/')" style="margin-top:2.6%;">Client: <span>TECH DOME PENANG, MALAYSIA</span></div>
<br>
<div class="pro_client" onClick="window.open('http://www.sciencealive.co.nz/')">IN ASSOCIATION WITH: <span>SCIENCE ALIVE, CHRISTCHURCH, New Zealand</span></div>

<div class="thumb_wrapper noselect">
		<a class="fancybox noselect" href="images/skeleton_img_1_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/skeleton_img_1.jpg" alt="" style="margin-left:0;" /></a>
		<a class="fancybox noselect" href="images/skeleton_img_2_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/skeleton_img_2.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/skeleton_img_3_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/skeleton_img_3.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/skeleton_img_4_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/skeleton_img_4.jpg" alt="" /></a>
		<!-- <a class="fancybox noselect" href="images/skeleton_img_5_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/skeleton_img_5.jpg" alt="" /></a> -->
		<a class="fancybox fancybox.iframe noselect" href="http://www.thestar.com.my/videos/2016/04/29/defy-newton-law-of-motion-at-tech-dome-penang/" data-fancybox-group="gallery"><img class="pro_thumb" src="images/skeleton_img_6.jpg" alt=""/></a>  
		<a class="fancybox fancybox.iframe noselect" href="http://www.youtube.com/embed/20eEwudC2wU" data-fancybox-group="gallery"><img class="pro_thumb" style="background:url(images/skeleton_img_5.jpg) no-repeat; background-size: cover;" src="images/play_icon.png" alt=""/></a>
	</div> <!-- thumb_wrapper -->
</div> <!-- pro_wrapper -->


	<!-- this block below is in the popup.css -->

		<div id="Popup">
		      <div class="pro_description" style="margin-left:auto; margin-right:auto; max-width:600px;">    	
					Our brief was to design & develop an interactive aimed at a younger audience, allowing them to gain an understanding of how a human skeleton moves, and the connection between the bones.
					<br><br>
					Using Kinect Technology as the primary interactive mechanism, the team at Inc Creative designed the interface - which displayed in both English and Malay languages, completed the rigging of the 3D model skeleton and developed the software to run it.
					<br><br>
					Our lead developer Mike Abernethy also travelled to Malaysia to install the interactive at the Tech Dome in Penang. Since it is a long way to travel for servicing and maintenance, he also developed software that also allows us to monitor the system remotely from NZ.
					<br><br>
					The Skelton Moves is an entertaining, educational and highly engaging interactive. In just a few moments of movement, you find yourself being fascinated by just how much of a physical work-of-art your body really is!
	      </div>

		      <div class="popupClose_wrapper"><a id="popupClose"><img class="popup_close_btn" src="images/popup_close.png"
					onmouseover="this.src='images/popup_close_hover.png';"
					onmouseout="this.src='images/popup_close.png';"></a>
			  </div>  
		  </div>  <!-- popup ends here -->
		<div id="bgPopup"></div> 

		<!-- this block above is in the popup.css -->


<video autoplay loop poster="vid/skeleton_bg_vid.jpg" id="bgvid">
<source src="vid/skeleton_bg_vid.webm" type="video/webm">
<source src="vid/skeleton_bg_vid.mp4" type="video/mp4">
</video>


<?php include_layout_template('fancy_box_popup.php') ?>

<?php include_layout_template('footer.php') ?>