<?php
require_once('../includes/init.php');
include_layout_template('header.php');
?>


	<div id="contact_wrapper" class="noselect">
		<div class="contact_title" >MAKE CONTACT,<br>WE WILL SORT YOU OUT!</div> 


		<ul class="contact_btn_wrapper">


<!-- 			<li class="contact_img_wrapper" style="margin-left: -30px;">
				<img class="contact_img" src="images/details_icon.png" href="mailto:martin@inccreative.asia" onmouseover="this.src='images/details_icon_hover.png';" onmouseout="this.src='images/details_icon.png';">
				<div class="contact_info">
					<span>martin</span><br>
						<div style="margin-top:22px;">+60 17 727 2388<br>martin@inccreative.asia</div>
				</div>
			</li>

			<li class="contact_img_wrapper">
				<img class="contact_img" src="images/details_icon.png" href="mailto:martin@inccreative.asia" onmouseover="this.src='images/details_icon_hover.png';" onmouseout="this.src='images/details_icon.png';">
				<div class="contact_info">
					<span>lim</span><br>
						<div style="margin-top:22px;">+64 21 140 7607<br>lim@inccreative.asia</div>
				</div>
			</li>

			<li class="contact_img_wrapper">
				<img class="contact_img" src="images/address_icon.png" onmouseover="this.src='images/address_icon_hover.png';" onmouseout="this.src='images/address_icon.png';" onClick="MyWindow=window.open('https://www.google.co.nz/maps/place/18A+Bell+St,+Whanganui,+Wanganui+4500/@-39.9298903,175.0541924,17z/data=!3m1!4b1!4m2!3m1!1s0x6d4003bb29ff77ef:0x3609aa63dc02aff5','MyWindow'); return false;">
				<div class="contact_info">
					<span>headquarters</span><br>
						<div style="margin-top:22px;">18A Bell Street<br>Wanganui 4500<br>New Zealand<br>+64 6 348 8073</div>
				</div>
			</li>

			<li class="contact_img_wrapper">
				<img class="contact_img" src="images/address_icon.png" onmouseover="this.src='images/address_icon_hover.png';" onmouseout="this.src='images/address_icon.png';" onClick="MyWindow=window.open('https://www.google.co.nz/maps/place/OverTime+IOI+Boulevard+Puchong/@3.046257,101.6217112,17z/data=!3m1!4b1!4m2!3m1!1s0x31cc4b5e4cbfce79:0xce07766d5bdf5768','MyWindow'); return false;">
				<div class="contact_info">
					<span>malaysia</span><br>
						<div style="margin-top:22px;">A-1-32 IOI Boulevard<br>Jalan Kenari 7<br>Bandar Puchong Jaya<br>47100 Puchong, Selangor<br>Malaysia<br>+60 3 8079 0918</div>
				</div>
			</li>
 -->

			<li class="contact_img_wrapper" style="margin-left:0;"><a class="contact_btn" href="skype:inccreative"><img class="contact_img" src="images/desk_phone.png" onmouseover="this.src='images/desk_phone_hover.png';" onmouseout="this.src='images/desk_phone.png';" alt="Quick Fact"></a><div class="contact_info">+64 21 140 7607<br>+64 6 348 8073</div></li>
			
			<li class="contact_img_wrapper"><a class="contact_btn" href="mailto:lim@inccreative.co.nz"><img class="contact_img" src="images/desk_email.png" onmouseover="this.src='images/desk_email_hover.png';" onmouseout="this.src='images/desk_email.png';" alt="Quick Fact"></a><div class="contact_info">lim@inccreative.co.nz<br>info@inccreative.co.nz</div></li>
			
			<li class="contact_img_wrapper"><a class="contact_btn" href="" onClick="MyWindow=window.open('https://www.google.co.nz/maps/place/53+Wilson+St,+Whanganui,+4500/@-39.9349805,175.0456315,17z/data=!3m1!4b1!4m5!3m4!1s0x6d4003be5e9263c9:0x5896e1456a195c6c!8m2!3d-39.9349846!4d175.0478202','MyWindow',width=320,height=240); return false;"><img class="contact_img" src="images/desk_address.png" onmouseover="this.src='images/desk_address_hover.png';" onmouseout="this.src='images/desk_address.png';" alt="Quick Fact"></a> <div class="contact_info">53D Wilson Street,<br>Wanganui 4500,<br>New Zealand.</div></li>
			
			<!-- <li class="contact_img_wrapper"><a class="contact_btn" href=""><img class="contact_img" src="images/desk_facebook.png" onmouseover="this.src='images/desk_facebook_hover.png';" onmouseout="this.src='images/desk_facebook.png';" alt="Quick Fact"></a><div class="contact_info">facebook/inccreative.com</div></li> -->

		</ul>

<?php include_layout_template('fancy_box_popup.php') ?>

<?php include_layout_template('footer.php') ?>