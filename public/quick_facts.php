<?php
require_once('../includes/init.php');
include_layout_template('header.php');
?>


<div class="clients_wrapper noselect">

				<div class="quick_fact_description">
					<h4>WE DO JUST ABOUT ANYTHING.<br>BUT WE'RE EXPERTS IN<span>_</span><br> INTERACTIVE DESIGN.</h4>
				</div>

				<div class="client_text">Clients we have worked with: </div>

				<div class="logo_wrapper">
					<img class="client_logo" src="images/client_logos/aa.jpg">
					<img class="client_logo" src="images/client_logos/am.jpg">
					<img class="client_logo" src="images/client_logos/anz-large.jpg">
					<!-- <img class="client_logo" src="images/client_logos/anz-netball-championships.jpg"> -->
					<img class="client_logo" src="images/client_logos/ASB_Bank_logo_(2004-2012).jpg">
					<img class="client_logo" src="images/client_logos/flyPalmy.jpg">
					<img class="client_logo" src="images/client_logos/fonterra.jpg">
					<img class="client_logo" src="images/client_logos/hyundai-logo.jpg">
					<img class="client_logo" src="images/client_logos/NAM_logo.jpg">
					<img class="client_logo" src="images/client_logos/NZFS.jpg">
					<img class="client_logo" src="images/client_logos/nzrm.jpg">
					<img class="client_logo" src="images/client_logos/Olex.jpg">
					<!-- <img class="client_logo" src="images/client_logos/Orange_Group_Logo.jpg"> -->
					<img class="client_logo" src="images/client_logos/Ports_of_AKL.jpg">
					<img class="client_logo" src="images/client_logos/puke-ariki.jpg">
					<img class="client_logo" src="images/client_logos/RWC2011.jpg">
					<img class="client_logo" src="images/client_logos/samsung-logo.jpg">
					<img class="client_logo" src="images/client_logos/screenVista.jpg">
					<!-- <img class="client_logo" src="images/client_logos/TAFT_logoMono.jpg"> -->
					<!-- <img class="client_logo" src="images/client_logos/temanawa.jpg"> -->
					<img class="client_logo" src="images/client_logos/te-papa-logo.jpg">
					<img class="client_logo" src="images/client_logos/techDome_Penang.jpg">
					<img class="client_logo" src="images/client_logos/teppapa-dpc.jpg">
					<!-- ======================== NEWLY ADDED AT 2019 OCT 23 ========================== -->
					<img class="client_logo" src="images/client_logos/suzuki-logo.jpg">
					<img class="client_logo" src="images/client_logos/npdc-logo.jpg">
					<img class="client_logo" src="images/client_logos/unoloco-logo.jpg">
					<img class="client_logo" src="images/client_logos/99-logo.jpg">
					<img class="client_logo" src="images/client_logos/pncc-logo.jpg">
					<!-- ========================Nexus Media Works Logo above=========================== -->
					<!-- <img class="client_logo" src="images/client_logos/1-kl-sogo.jpg">
					<img class="client_logo" src="images/client_logos/2-air-asia.jpg">
					<img class="client_logo" src="images/client_logos/3-eghl.jpg">
					<img class="client_logo" src="images/client_logos/4-tm-logo.jpg">
					<img class="client_logo" src="images/client_logos/5-minecruise.jpg">
					<img class="client_logo" src="images/client_logos/6-hong-leoang-assurance-logo.jpg">
					<img class="client_logo" src="images/client_logos/7-OCBC_Bank_Logo.svg.jpg">
					<img class="client_logo" src="images/client_logos/8-life-care.jpg">
					<img class="client_logo" src="images/client_logos/9-Birkenstock_logo.jpg">
					<img class="client_logo" src="images/client_logos/10-desntsu.jpg"> -->
				</div>

				<div class="quick_fact_contact">
					CONTACT US NOW :<br>
<!-- 					 <div class="footer_btn_wrapper">
					 MARTIN :  <a class="footer_btn" href="skype:inccreative">+60 17 7272388</a>   /  <a class="footer_btn" href="mailto:lim@inccreative.co.nz">martin@inccreative.asia</a>
					 </div> -->
					 <div style="padding-top:5px;">
					 LIM :  <a class="footer_btn" href="skype:inccreative">+64 21 1407607</a>   /  <a class="footer_btn" href="mailto:lim@inccreative.co.nz">lim@inccreative.co.nz</a>
					 </div>
				</div>

			</div>

<?php include_layout_template('footer.php') ?>
