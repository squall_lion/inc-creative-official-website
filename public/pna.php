<?php
require_once('../includes/init.php');
include_layout_template('header.php');
?>


<div id="pro_wrapper" class="noselect" style="z-index:1;">
				<div class="pro_title">E-OLOGY<br>ADVERTISING SCREENS
				<div class="pro_sub_title">65" TOUCHSCREEN KIOSKS,<br>75" DISPAY SCREEN DESIGN,<br>INSTALL & MAINTENANCE</div></div>
				<!-- <div class="pro_category">65" & 75" Display panel</div>  -->
				

<div class="pro_description">
	Our sister company, E-ology is in the business of venue advertising...<a id="myButton" href="#"><span> READ MORE</span></a>
</div>
<div class="pro_client" onClick="window.open('http://www.pnairport.co.nz/')" style="margin-top:2.6%;">Client: <span>Palmerston North Airport Limited, New Zealand</span></div>
<br>
<div class="pro_client" onClick="window.open('http://www.e-ology.co.nz/')">Partner: <span>E-ology Limited</span></div>

<div class="thumb_wrapper noselect">
		<a class="fancybox noselect" href="images/pna_img_1_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/pna_img_1.jpg" alt="" style="margin-left:0;" /></a>
		<a class="fancybox noselect" href="images/pna_img_2_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/pna_img_2.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/pna_img_3_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/pna_img_3.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/pna_img_4_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/pna_img_4.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/pna_img_5_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/pna_img_5.jpg" alt="" /></a>
		<a class="fancybox noselect" href="images/pna_img_6_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" src="images/pna_img_6.jpg" alt="" /></a>
		<!-- <a class="fancybox fancybox.iframe noselect" href="http://www.youtube.com/embed/XNmnFzjXUgU" data-fancybox-group="gallery"><img class="pro_thumb" style="background:url(images/gal_vid.jpg) no-repeat; background-size: cover;" src="images/play_icon.png" alt=""/></a>  -->
	</div> <!-- thumb_wrapper -->
</div> <!-- pro_wrapper -->


	<!-- this block below is in the popup.css -->

		<div id="Popup">
		      <div class="pro_description" style="margin-left:auto; margin-right:auto; max-width:600px;">
		      	Our sister company, E-ology is in the business of venue advertising. Having recently taken over the management of the advertising inside the terminal at Palmerston North Airport, E-ology relies on Inc's expertise to supply, design, develop and maintain all of its screen advertising platforms. From large display screens to touchscreen kiosks, Inc is kept busy continually developing everything from the 'Apps' that the screens use to display the advertising, through to a full backend media upload CMS and individual screen monitoring systems. Inc Creative will continue to push the boundaries of how advertisers will engage with their audience... or what E-ology calls 'Engagement - technology.'
		      </div>

		      <div class="popupClose_wrapper"><a id="popupClose"><img class="popup_close_btn" src="images/popup_close.png"
					onmouseover="this.src='images/popup_close_hover.png';"
					onmouseout="this.src='images/popup_close.png';"></a>
			  </div>  
		  </div>  <!-- popup ends here -->
		<div id="bgPopup"></div> 

		<!-- this block above is in the popup.css -->


<video autoplay loop poster="vid/pna_bg_vid.jpg" id="bgvid">
<source src="vid/pna_bg_vid.webm" type="video/webm">
<source src="vid/pna_bg_vid.mp4" type="video/mp4">
</video>


<?php include_layout_template('fancy_box_popup.php') ?>

<?php include_layout_template('footer.php') ?>