<?php
require_once('../includes/init.php');
include_layout_template('header.php');

$project_title="Black Exhibition @ Toyota City, RWC 2019";
$project_subtitle="Stories of New Zealand & Japan rugby connections";
$project_category="Exhibition design, all graphics - digital and print";

$project_client="New Zealand Rugby Museum, Palmerston North & Toyota City, Aichi, Japan";
$project_client_url="#";

$projectName_img = "jpn_exhibition";
$project_YouTube_link = "";

$project_description_short="";

$project_description_full="";

?>


<div id="pro_wrapper" class="noselect" style="z-index:1;">
				<div class="pro_title"><?php echo $project_title; ?><br>
				<div class="pro_sub_title"><?php echo $project_subtitle; ?></div>
				</div>
				<div class="pro_category"><?php echo $project_category; ?></div>


<!-- <div class="pro_description"><?php 	//echo $project_description_short; ?><a id="myButton" href="#"><span> READ MORE</span></a></div> -->

<!-- <div class="pro_client" onClick="window.open('<?php echo $project_client_url; ?>')" style="margin-top:2.6%;">Client: <span><?php echo $project_client ?></span></div> -->
<div class="pro_client" style="margin-top:2.6%;">Client: <span><?php echo $project_client ?></span></div>

<div class="thumb_wrapper noselect">
  <a class="fancybox noselect" href="images/<?php echo $projectName_img; ?>_img_001_large.jpg" data-fancybox-group="gallery">
		<img class="pro_thumb" style="background:url(images/<?php echo $projectName_img; ?>_img_001_large.jpg) no-repeat; background-size: cover;" src="images/null.png"/>
	</a>
  <!-- <a class="fancybox noselect" href="images/<?php echo $projectName_img; ?>_img_002_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" style="background:url(images/<?php echo $projectName_img; ?>_img_002_large.jpg) no-repeat; background-size: cover;" src="images/null.png"/></a> -->
  <a class="fancybox noselect" href="images/<?php echo $projectName_img; ?>_img_003_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" style="background:url(images/<?php echo $projectName_img; ?>_img_003_large.jpg) no-repeat; background-size: cover;" src="images/null.png"/></a>
  <a class="fancybox noselect" href="images/<?php echo $projectName_img; ?>_img_008_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" style="background:url(images/<?php echo $projectName_img; ?>_img_008_large.jpg) no-repeat; background-size: cover;" src="images/null.png"/></a>
  <a class="fancybox noselect" href="images/<?php echo $projectName_img; ?>_img_004_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" style="background:url(images/<?php echo $projectName_img; ?>_img_004_large.jpg) no-repeat; background-size: cover;" src="images/null.png"/></a>
  <a class="fancybox noselect" href="images/<?php echo $projectName_img; ?>_img_005_large.jpg" data-fancybox-group="gallery"><img class="pro_thumb" style="background:url(images/<?php echo $projectName_img; ?>_img_005_large.jpg) no-repeat; background-size: cover;" src="images/null.png"/></a>
  <!-- <a class="fancybox noselect" href="images/<?php echo $projectName_img; ?>_img_006_large.jpg" data-fancybox-group="gallery"> -->
  <a class="fancybox noselect fancybox.iframe" href="replica_n.html" data-fancybox-group="gallery">
		<img class="pro_thumb" style="background:url(images/<?php echo $projectName_img; ?>_img_007_large.jpg) no-repeat; background-size: cover;" src="images/null.png"/>
	</a>
		<!-- <a class="fancybox fancybox.iframe noselect" href="http://www.youtube.com/embed/<?php echo $project_YouTube_link;?>" data-fancybox-group="gallery"><img class="pro_thumb" style="background:url(images/<?php echo $projectName_img; ?>_img_001_large.jpg) no-repeat; background-size: cover;" src="images/play_icon.png" alt=""/></a> -->
	</div> <!-- thumb_wrapper -->
</div> <!-- pro_wrapper -->

	<!-- this block below is in the popup.css -->
		<!-- <div id="Popup">
		      <div class="pro_description" style="margin-left:auto; margin-right:auto; max-width:600px;">
		      	<?php //	echo $project_description_full; ?>
		      </div>

		      <div class="popupClose_wrapper"><a id="popupClose"><img class="popup_close_btn" src="images/popup_close.png"
					onmouseover="this.src='images/popup_close_hover.png';"
					onmouseout="this.src='images/popup_close.png';"></a>
			  </div>
		  </div>
		<div id="bgPopup"></div> -->
    <!-- popup ends here -->
		<!-- this block above is in the popup.css -->


    <!-- <video autoplay loop poster="vid/<?php echo $projectName_img; ?>_bg_vid.jpg" id="bgvid"> -->
<video autoplay loop muted id="bgvid">
<source src="vid/jpn_exhibition_slideshow.mp4" type="video/mp4">
</video>


<?php include_layout_template('fancy_box_popup.php') ?>

<?php include_layout_template('footer.php') ?>
